��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  )   �      �   *  �   �  �   �     _     g     �     �     �     �      �     �  *     u   8     �  !  �    �  �   �   G  �!     A#     `#  �   ~#  �   b$  �   �$  �   �%     t&  
   �&     �&     �&  q   �&     3'     E'  �   V'  ~   �'     n(  �  �(  g  '*     �,  -   �,  ,   �,  �   
-  
   �-  +   �-  �   .  �   �.  w  K/     �0            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Romanian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Acțiune Fereastra de acțiune: Adăugați Date@Time pentru a nota textul După ce imaginea și degetul mare sunt create, apare o fereastră de acțiune în care îi spuneți
	 ce acțiuni să întreprindeți cu fotografia dvs. În partea de sus, va afișa dacă captura de ecran a fost salvată sau nu și calea rezultată și numele fișierului. După ce fotografia este făcută, dacă este de pe întregul ecran, aceasta va fi redusă
	 la 85%, astfel încât să se potrivească în fereastra de editare și să fie mai ușor de editat. O aplicație care face o fotografie a ecranului dvs., vă permite
\să o previzualizați, să o adnotați și să o editați și să o salvați, precum și să o inserați în sau
\să deschideți cu alte aplicații. O fereastră de dialog inițială vă permite să spuneți aplicației ce doriți. Schimbați\în\opțiunile dacă este necesar, apoi faceți clic pe Ok. Înapoi Alegeți un director diferit Creați o miniatură Selectare cursor Șterge pagina: Fereastra de dialog: Editare și adnotare cu MTPaint: Mesaj de eroare de la imgur: Fișierul nu există; săriți peste $file Fișier salvat, gata pentru a efectua procesul selectat, captură de ecran salvată ca: $SAVEDIR/$file_name.$file_ext Ecran complet Dacă este selectată, fotografia este copiată în clipboard, astfel încât să o puteți lipi direct
	 în alte aplicații (cum ar fi ecranele de scriere a e-mailurilor de e-mail Yahoo sau AOL) sau documente
	 precum LibreOffice Writer etc., fără a fi nevoie să atașați un fișier. Dacă deschideți folderul într-un manager de fișiere, puteți face clic dreapta pe fișiere pentru a vedea
	 ce opțiuni va permite. Majoritatea managerilor de fișiere vă vor permite să adăugați sau

u modificați opțiunile programului dacă acestea nu apar așa cum doriți. Dacă înregistrați un ID API Client IMGUR, îl puteți introduce pe al dvs. în caseta
	 furnizată, înlocuind valoarea implicită dată. Dacă aveți probleme cu IMGUR

u funcționează des, încercați să vă înregistrați propriul ID API Client. În modul fereastră, dacă aveți probleme cu coruperea ecranului (zzzFM sau SpaceFM
	 desktopul și Fluxbox pot prezenta probleme), încercați să deselectați chenarele ferestrei
	 pentru a vedea dacă vă ajută. O opțiune pentru a include cursorul mouse-ului în imaginea dvs. este disponibilă și în modul fereastră. Includeți cursorul mouse-ului Includeți marginea ferestrei Vă poate salva fotografia în diferite tipuri de fișiere, limitate la cele acceptate
	 de aplicația de fotografiere subiacentă, Scrot și MTPaint, programul folosit pentru a
	 vizualiza, adnota și edita capturile de ecran. Poate fotografia o zonă vizibilă pe care o selectați cu cursorul mouse-ului,
\o fereastră vizibilă sau întregul ecran cu întârziere. Are opțiunea de a crea degetul mare pentru imaginea dvs. și creează degetul mare
	 după ce faceți adnotarea și editarea inițială cu MTPaint. Este permis
	 să creați un degetul mare peste 100% din dimensiunea originală. În mod implicit, va crea și utiliza un dosar Capturi de ecran în directorul dvs. de pornire
	, dar îl puteți schimba într-un alt director dacă doriți. LibreOffice nu este instalat. Deschidere Regiunea de capturat Salvare stare: Salvarea și deschiderea cu MTPaint deschide atât captura de ecran, cât și degetul mare dacă a fost creat


. Captură de ecran Capturi de ecran Unele aplicații din meniu sunt denumite generic, deoarece aplicația reală care




 run este implicită curentă selectată în Aplicații preferate. Imaginea se va deschide apoi în MTPaint pentru a previzualiza, edita și adnota. Toate
	 funcțiile MTPaint sunt disponibile. Miniatură pentru pagini web Pentru a adăuga text suplimentar la imaginea dvs., faceți clic pe pictograma T și introduceți textul
	 în câmpul furnizat. Puteți ajusta dimensiunea fontului și atributele dacă nu doriți. Faceți clic pe butonul Lipire text când ați terminat. Apoi selectați și trageți textul în
	 unde doriți să fie poziționat. Dacă nu este corect, apăsați Ctrl-z pentru a anula și încercați
	 din nou. Pentru a desena o linie cu o săgeată la sfârșit, dați clic pe pictograma Linie dreaptă care
	 este o riglă, apoi indicați unde ar trebui să înceapă linia și faceți clic, apoi trageți
	 până unde acea secțiune a liniei ar trebui să se termine. Eliberați butonul mouse-ului pentru a

 termina acea secțiune a liniei. Puteți adăuga o altă secțiune făcând clic și

u trăgând din nou. După ce secțiunea de linie este desenată, puteți apăsa tasta A
	 și va transforma capătul liniei într-un vârf de săgeată. Apoi apăsați tasta Esc
	 pentru a termina modul de desenare a liniilor. Încărcarea în IMGUR a eșuat Se încarcă adresa URL în fișierul IMGUR $ Se încarcă fișierul în fișierul IMGUR $ După ce ați terminat editarea și adnotarea, faceți clic pe butonul Salvare și apoi pe butonul Închide
	. După ce ieși, degetul mare va fi generat automat din captura de ecran
	 salvată. Fereastră Fereastra TREBUIE să fie complet vizibilă De asemenea, aveți opțiunea de a anula sau de a ieși, de a face o altă captură de ecran
	 sau de a salva fișierul și de a ieși. Puteți alege numele de bază al fișierelor pe care doriți să le creați în limitele sistemului
	 și va sufix automat numele cu data și ora curente pentru a vă asigura că va fi unic. Puteți adăuga automat o notă text la captură de ecran, opțional
	 cu Data:Ora curentă adăugată și ca sufix la șirul dvs. de text.
	 Când apare pe ecranul MTPaint, pur și simplu trageți textul în locul în care nu îl doriți pe captură de ecran. Dacă se dorește, fontul, dimensiunea, poziția și culoarea,
	 etc pot fi alese în dialogul MTPaint Paste Text. captură de ecran 