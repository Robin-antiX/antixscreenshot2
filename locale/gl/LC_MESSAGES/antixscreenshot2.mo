��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  !   �  �   �  �   �  �   l  �        �     �     �     �     �       "        ;  %   U  }   {     �      �     �      )  !     9"     S"  �   m"  |   D#  �   �#  �   �$      %     <%     E%     X%  b   g%     �%     �%  �   �%     �&     '  q  5'  )  �(  $   �*  "   �*     +  �   9+     �+  )   �+  j   %,  �   �,  �  T-     �.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Galician (https://www.transifex.com/antix-linux-community-contributions/teams/121548/gl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl
Plural-Forms: nplurals=2; plural=(n != 1);
 Acción Fiestra de acción: Engade Data@Hora ao texto da nota Despois de crear a imaxe e o polgar, aparecerá unha xanela de acción onde lle indicas
	 que accións debes realizar coa túa foto. Na parte superior mostrará se a captura de pantalla foi gardada ou non, e a ruta e o nome do ficheiro resultantes. Despois de tirar a foto, se é a pantalla completa, reducirase
	 ao 85 % para que encaixe na xanela de edición e sexa máis fácil de editar. Unha aplicación que toma unha foto da túa pantalla, permíteche
\previsualizala, anotala e editala e gardala, ademais de pegala ou
\para abrir con outras aplicacións. Unha xanela de diálogo inicial permítelle dicir á aplicación o que quere. Cambia
as\as opcións se é necesario e fai clic en Aceptar. De volta Escolla un directorio diferente Crea unha miniatura Cursor Seleccionar Eliminar páxina: Fiestra de diálogo: Edición e anotación con MTPaint: Mensaxe de erro de imgur: O ficheiro non existe; saltando $file Ficheiro gardado, listo para realizar o proceso seleccionado, captura de pantalla gardada como: $SAVEDIR/$file_name.$file_ext Pantalla completa Se se selecciona, a imaxe cópiase no portapapeis para que poida pegala directamente
	 noutras aplicacións (como pantallas de redacción de correo electrónico de Yahoo ou AOL) ou documentos

 como LibreOffice Writer, etc., sen necesidade de anexar un ficheiro. Se abres o cartafol nun xestor de ficheiros, podes facer clic co botón dereito nos ficheiros para ver
	 que opcións permitirá. A maioría dos xestores de ficheiros permítenche engadir ou

 cambiar as opcións do programa se non aparecen como queres. Se rexistras un ID de API de cliente IMGUR, podes introducir o teu propio na caixa
	 proporcionada substituíndo o valor predeterminado indicado. Se tes problemas con IMGUR

 que non funciona a miúdo, proba a rexistrar o teu propio ID da API de cliente. No modo ventá, se tes problemas de corrupción da pantalla (zzzFM ou SpaceFM
	 o escritorio e Fluxbox poden presentar problemas), proba a desmarcar os bordos da ventá
	 para ver se axuda. Unha opción para incluír o propio cursor do rato na túa imaxe
	 tamén está dispoñible no modo Ventá. Incluír o cursor do rato Inclúe o bordo da xanela Pode gardar a túa imaxe en varios tipos de ficheiros, limitados aos admitidos
	 pola aplicación de toma de imaxes subxacente, Scrot e MTPaint, o programa usado para
	 ver, anotar e editar as capturas de pantalla. Pode sacar a foto dunha área visible que selecciones co cursor do rato,
\unha xanela visible ou toda a pantalla cun atraso. Ten a opción de crear un polgar para a túa foto e crea o polgar
	 despois de facer a túa anotación e edición inicial con MTPaint. É permitido
	 crear un polgar máis do 100 % do tamaño orixinal. Creará e usará por defecto un cartafol Capturas de pantalla no teu directorio
	 de inicio, pero podes cambialo a outro directorio se queres. LibreOffice non está instalado. Apertura Rexión a capturar Gardar estado: Gardar e abrir con MTPaint abre tanto a captura de pantalla como o polgar se

 se creou un polgar. Captura de pantalla Capturas de pantalla Algunhas aplicacións do menú reciben un nome xenérico porque a aplicación real que non se executará é a predeterminada seleccionada en Aplicacións preferidas. A imaxe abrirase en MTPaint para que poida previsualizar, editar e anotar. Todas
	 as funcións de MTPaint están dispoñibles. Miniatura para páxinas web Para engadir texto adicional á túa imaxe, fai clic na icona T e introduce o texto
	 no campo proporcionado. Podes axustar o tamaño e os atributos da fonte se non o desexas. Fai clic no botón Pegar texto cando remate. A continuación, selecciona e arrastra o texto ata
	 onde queres que se coloque. Se non é correcto, preme Ctrl-z para desfacer e téntao
	 de novo. Para debuxar unha liña cunha frecha ao final fai clic na icona de liña recta que
	 é unha regra e, a continuación, sinala onde debe comezar a liña e fai clic e arrastra
	 ata onde esa sección da liña debería rematar. Solta o botón do rato para
	 finalizar esa sección da liña. Podes engadir outra sección facendo clic e

 arrastrando de novo. Despois de debuxar a sección da liña, podes premer a tecla A
	 e converterá o final da liña nunha punta de frecha. A continuación, preme a tecla Esc
	 para finalizar o modo de debuxo de liñas. Produciuse un erro ao cargar a IMGUR Cargando o URL no ficheiro IMGUR $ Cargando ficheiro a IMGUR $file Cando remate de editar e anotar, fai clic no botón Gardar e despois no botón Pechar
	. Despois de saír, o teu polgar xerarase automaticamente a partir da captura de pantalla gardada. Fiestra A xanela DEBE estar completamente visible Tamén tes a opción de cancelar ou saír, facer outra
	 captura de pantalla ou gardar o ficheiro e saír. Podes escoller o nome base dos ficheiros que queres crear dentro das limitacións do sistema
	 e sufixará automaticamente o nome coa data
	 e a hora actuales para asegurarte de que será único. Podes facer que engada automaticamente unha nota de texto á túa captura de pantalla, opcionalmente
	 coa Data:Hora actual engadida tamén como sufixo á cadea de texto.
	 Cando apareza na pantalla de MTPaint, simplemente arrastra o texto onde


 queres na captura de pantalla. Se o desexa, pódese escoller o tipo de letra, o tamaño, a posición e a cor
	 etc no diálogo Pegar texto de MTPaint. captura de pantalla 