��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  &     �   /  �     �   �  �   V     �     �                (     ;  -   N     |  &   �  }   �     @  �   L    J  �   S   ?  -!     m"     �"  �   �"  ~   �#  �   $  �   �$     c%     �%     �%     �%  T   �%      &     &  �   &  �   �&     K'  k  d'    �(     �*     �*     +  �   <+     �+      �+  �   ,  �   �,  {  i-     �.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Croatian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/hr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Akcijski Prozor za radnju: Dodajte datum@vrijeme tekstu bilješke Nakon što su slika i palac stvoreni, pojavljuje se prozor s radnjom u kojem 
	 kažete koje radnje poduzeti sa svojom slikom. Na vrhu će prikazati je li

 snimka zaslona spremljena ili ne, te rezultirajući put i naziv datoteke. Nakon snimanja slike, ako je na cijelom zaslonu, smanjit će se
	 na 85% tako da će stati u prozor za uređivanje i biti lakše za uređivanje. Aplikacija koja snima sliku vašeg zaslona, omogućuje vam
	 da je pregledate, označite i uredite, te spremite, kao i da je zalijepite u ili
\otvorite s drugim aplikacijama. Početni dijaloški prozor omogućuje vam da aplikaciji kažete što želite. Promijenite\u\opcije ako je potrebno, a zatim kliknite U redu. leđa Odaberite drugi imenik Napravite minijaturu Odabir pokazivača Izbriši stranicu: Dijaloški prozor: Uređivanje i označavanje pomoću MTPaint-a: Poruka o pogrešci od imgur-a: Datoteka ne postoji; preskakanje $file Datoteka je spremljena, spremna za izvođenje odabranog procesa, snimka zaslona spremljena kao: $SAVEDIR/$file_name.$file_ext Puni zaslon Ako je odabrana, slika se kopira u međuspremnik tako da je možete zalijepiti izravno
	 u druge aplikacije (kao što su zasloni za sastavljanje e-pošte Yahoo ili AOL) ili dokumente
	 poput LibreOffice Writer, itd., bez potrebe za prilaganjem datoteka. Ako otvorite mapu u upravitelju datoteka, možete desnom tipkom miša kliknuti datoteke da vidite
	 koje opcije će dopustiti. Većina upravitelja datoteka omogućit će vam da dodate ili
	 promijenite opcije programa ako se ne prikazuju onako kako biste željeli. Ako registrirate IMGUR Client API ID, možete unijeti svoj vlastiti u predviđeni okvir
	 nadjačavajući zadanu vrijednost. Ako imate problema s IMGUR
	 ne radi često, pokušajte registrirati vlastiti Client API ID. U načinu rada Window, ako imate problema s oštećenjem zaslona (zzzFM ili SpaceFM
	 radna površina i Fluxbox mogu predstavljati probleme), pokušajte poništiti odabir okvira prozora
	 da vidite hoće li to pomoći. Opcija uključivanja samog pokazivača miša u vašu
	 sliku također je dostupna u načinu prozora. Uključuje pokazivač miša Uključite obrub prozora Može spremiti vašu sliku u različite vrste datoteka, ograničeno na one koje podržava
	 osnovna aplikacija za snimanje slika, Scrot i MTPaint, program koji se koristi za
	 pregledavanje, označavanje i uređivanje snimaka zaslona. Može snimiti sliku vidljivog područja koje odaberete pokazivačem miša,
	a vidljivog prozora ili cijelog zaslona s odgodom. Ima opciju stvaranja palca za vašu sliku, a stvara palac
	 nakon što izvršite svoje početno označavanje i uređivanje s MTPaint-om. Dopušteno je
	 stvoriti palac veći od 100% izvorne veličine. Zadano će stvoriti i koristiti mapu Screenshots u vašem početnom
	 direktoriju, ali možete je promijeniti u drugi direktorij ako želite. LibreOffice nije instaliran. Otvor Regija za snimanje Status spremanja: Spremi i otvori s MTPaint-om otvara i snimku zaslona i palac ako je palac

 stvoren. Snimka zaslona Snimke zaslona Neke se aplikacije na izborniku nazivaju općenito jer je stvarna aplikacija koja

eće se pokrenuti trenutačno zadana vrijednost odabrana u Preferiranim aplikacijama. Slika će se zatim otvoriti u MTPaintu kako biste je mogli pregledati, urediti i komentirati. Dostupne su sve
	 značajke MTPainta. Sličica za web stranice Da biste svojoj slici dodali dodatni tekst, samo kliknite na ikonu T i unesite tekst
	 u predviđeno polje. Možete prilagoditi veličinu fonta i atribute ako
	 želite. Kada završite, kliknite gumb Zalijepi tekst. Zatim odaberite i povucite tekst na
	 mjesto gdje želite da se postavi. Ako nije ispravno, pritisnite Ctrl-z da poništite i pokušajte
	 ponovno. Da biste nacrtali liniju sa strelicom na kraju, kliknite ikonu Ravna linija koja
	 je ravnalo, a zatim pokažite na mjesto gdje bi linija trebala početi i kliknite, a zatim povucite
	 do mjesta na kojem je taj dio linije trebao završiti. Pustite tipku miša da
	 završite taj dio retka. Možete dodati još jedan odjeljak klikom i
	 ponovnim povlačenjem. Nakon što je dio linije nacrtan, možete pritisnuti tipku A
	 i to će pretvoriti kraj linije u vrh strelice. Zatim pritisnite tipku Esc
	 za završetak načina crtanja. Prijenos na IMGUR nije uspio Prijenos URL-a u $file IMGUR Prijenos datoteke u IMGUR $file Kada završite s uređivanjem i označavanjem, kliknite gumb Spremi, a zatim gumb Zatvori
	. Nakon što izađete, vaš palac će se automatski generirati iz
	 spremljene snimke zaslona. Prozor Prozor MORA biti potpuno vidljiv Također imate mogućnost jednostavnog otkazivanja ili izlaska, snimanja druge
	 snimke zaslona ili spremanja datoteke i izlaska. Možete odabrati osnovni naziv datoteka koje želite kreirati unutar sistemskih
	 ograničenja i automatski će nazivu dodati trenutni datum
	 i vrijeme kako biste bili sigurni da će biti jedinstven. Možete automatski dodati tekstualnu bilješku vašoj snimci zaslona, opcionalno
	 s trenutnim datumom:vrijeme koji je također dodat kao sufiks vašem tekstualnom nizu.
	 Kada se pojavi na zaslonu MTPainta, jednostavno povucite tekst gdje


 želite na snimci zaslona. Po želji, font, veličina, položaj i boja,
	 itd. mogu se odabrati u dijaloškom okviru MTPaint Paste Text. snimka zaslona 