��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  (   �  �   �  �   �  �   �  �   (     �      �     �     �     �       $        A  *   ^  g   �     �  �   �    �  �      =  �      7"     V"  �   v"  �   L#  �   �#  �   �$  #   L%     p%     y%     �%  \   �%  
   �%     &  �   &  u   �&     )'  �  D'  Q  �(     %+     C+  "   `+  �   �+     ?,  +   H,  h   t,  �   �,  �  �-  
   7/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Corsican (https://www.transifex.com/antix-linux-community-contributions/teams/121548/co/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: co
Plural-Forms: nplurals=2; plural=(n != 1);
 Azzione Finestra d'azzione: Aghjunghjite Date@Time per nutà u testu Dopu chì a foto è u pollice sò creati, una finestra d'Azzione si apre in quale dite 
	 ciò chì azzione piglià cù a vostra foto. In cima, mostrarà se a screenshot hè stata salvata o micca, è u percorsu risultatu è u nome di u schedariu. Dopu chì a foto hè stata presa, s'ellu hè di a schermu sanu, serà ridutta 
	 à 85% in modu chì si mette in a finestra di edizione è serà più faciule per edità. Una app chì piglia una foto di u vostru schermu, vi permette di vede in anteprima, annotà è edità, è salvà, è ancu incollà in o
\apri cù altre app. Una finestra di dialogu iniziale permette di dì à l'app ciò chì vulete. Cambia\in\l'opzioni se ne necessariu, dopu cliccate Ok. Torna Sceglite un cartulare differente Crea una miniatura Cursor Select Sguassà a pagina: Finestra di dialogu: Edizione è annotazione cù MTPaint: Missaghju d'errore da imgur: U schedariu ùn esiste micca; saltà $file File salvatu, Pronta à fà u prucessu sceltu, screenshot salvatu cum'è: $SAVEDIR/$file_name.$file_ext Screen Full Se selezziunata, a foto hè copiata in u clipboard in modu chì pudete incollà direttamente
	 in altre app (cum'è Yahoo o AOL mail cumpone schermi) o documenti
	 cum'è LibreOffice Writer, etc., senza avè bisognu di attache. un schedariu. Sè avete apertu u cartulare in un gestore di file, pudete cliccà cù u dirittu nantu à i schedari per vede
	 quale opzioni permetterà. A maiò parte di i gestori di fugliali vi permettenu di aghjunghje o
	 cambià l'opzioni di u prugramma s'ellu ùn appare micca cumu vulete. Se registrate un ID API Client IMGUR, pudete inserisce u vostru propiu in a casella 
	 furnita sopra u valore predeterminatu datu. Sì avete prublemi cù IMGUR 

 ùn funziona micca spessu, pruvate à registrà u vostru propiu Client API Id. In u modu Finestra, sè avete prublemi di corruzzione di u screnu (zzzFM o SpaceFM
	 desktop è Fluxbox ponu presentà prublemi), pruvate di diselezzione di i cunfini di a finestra 
	 per vede s'ellu aiuta. Un'opzione per include u cursore di u mouse stessu in a vostra foto
	 hè ancu dispunibule in u modu Finestra. Includite u cursore di u mouse Includite u bordu di a finestra Puderà salvà a vostra foto in diversi tipi di schedari, limitati à quelli supportati
	 da l'app per piglià foto sottostanti, Scrot è MTPaint, u prugramma utilizatu per
	 vede, annotà è edità i screenshots. Pò piglià a foto di una zona visibile chì selezziunate cù u cursore di u mouse, 
	a Finestra visibile, o tutta a pantalla in un ritardu. Hà l'opzione di creà un pollice per a vostra foto, è crea u pollice
	 dopu avè fattu a vostra annotazione iniziale è edità cù MTPaint. Hè permessu
	 di creà un pollice più di 100% di a dimensione originale. Serà predeterminatu per creà è aduprà un cartulare di Screenshots in u vostru cartulare di casa
	, ma pudete cambià in un altru cartulare se vulete. LibreOffice ùn hè micca stallatu. Apertura Regione da catturà Salvà Status: Salvà è apre cù MTPaint apre a screenshot è u pollice se un pollice hè statu 
	 creatu. Screenshot Screenshots Alcune app in u menù sò chjamate genericamente perchè l'app attuale chì 













































 A foto si apre in MTPaint per vede in anteprima, edità è annotà. Tutte
	 di e funzioni di MTPaint sò dispunibili. Miniatura per e pagine web Per aghjunghje Testu supplementu à a vostra foto, basta à cliccà l'icona T è inserite u testu 
	 in u campu furnitu. Pudete aghjustà a dimensione di u font è l'attributi se ùn vulete. Cliccate u buttone Paste Text quandu avete finitu. Allora selezziunate è trascinate u testu in 
	 induve vulete posizionatu. S'ellu ùn hè micca ghjustu, appughjà Ctrl-z per annullà è pruvà 
	 di novu. Per disegnà una linea cù una freccia à a fine, cliccate nantu à l'icona di Linea Retta chì
	 hè un Regnu, è dopu indicà induve a linea deve principià è cliccate, dopu trascinate
	 à induve quella sezione di a linea. deve finisce. Lasciate u buttone di u mouse per
	 finisce quella sezione di a linea. Pudete aghjunghje un'altra sezione clicchendu è
	 trascinendu di novu. Dopu chì a seccione di a linea hè disegnata, pudete appughjà a chjave A \ n \ t è trasfurmà a fine di a linea in una punta di freccia. Dopu appughjà a chjave Esc
	 per finisce u modu di disegnu di linea. Caricà à IMGUR hà fiascatu Uploading URL à IMGUR $file Caricà u schedariu à IMGUR $file Quandu hà finitu l'edità è l'annotazione, cliccate u buttone Salvà è dopu u buttone Chiudi
	. Dopu chì esce, u vostru pollice serà automaticamente generatu da a screenshot salvatu. Finestra A finestra DEVE esse completamente visibile Avete ancu l'opzione di solu annullà o esce, piglià un'altra screenshot, o salvà u schedariu è esce. Pudete sceglie u nome di basa di i fugliali chì vulete creà in i limiti di u sistema
	 è automaticamente suffissi u nome cù a data attuale 
	 è l'ora per esse sicuru chì serà unicu. Puderete aghjustà automaticamente una nota di testu à a vostra screenshot, optionally
	 cù l'attuale Data:Time aghjuntu ancu cum'è suffissu à a vostra stringa di testu.
	 Quandu vene nantu à a schermu MTPaint, simpricimenti trascinate. u testu à induve ùn vulete micca nantu à a screenshot. Se vulete, u font, a dimensione, a pusizione è u culore,
	 ecc pò esse sceltu in u dialogu MTPaint Paste Text. screenshot 