��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  )   �    �  �   �  �   �  �   [     �     �               +     9  "   T  #   w  %   �  x   �     :    N    i  �   }   '  Z!     �"     �"  �   �"  �   �#  �   /$  �   %     �%     �%     �%     �%  g   �%     A&     T&  �   c&  y   �&     g'  x  �'    �(  $   +     *+     I+  �   i+     *,  *   3,  {   ^,  �   �,  Z  �-     �.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Xhosa (https://www.transifex.com/antix-linux-community-contributions/teams/121548/xh/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: xh
Plural-Forms: nplurals=2; plural=(n != 1);
 Isenzo Ifestile yesenzo: Yongeza Umhla @ Ixesha ukuqaphela umbhalo Emva kokuba i-pic kunye nobhontsi benziwe, i-window yesenzo iyavela apho uxelele khona ukuba ngawaphi amanyathelo ekufuneka uwathathe ngomfanekiso wakho. Phezulu iya kubonisa ukuba 
	 umfanekiso wekhusi ugciniwe okanye hayi, kunye nesiphumo sendlela kunye negama lefayile. Emva kokuba i-pic ithathiwe, ukuba yeyesikrini esigcweleyo, iya kuthotywa ezantsi
	 ukuya kuma-85% ngoko ke iyakungena kwifestile yokuhlela kwaye kube lula ukuyihlela. I-app ethatha umfanekiso wesikrini sakho, ikuvumela ukuba



uyijonge kwangaphambili, uyichasise kwaye uyihlele, kwaye uyigcine, kwaye uyincamathisele kuyo okanye
\uyivule nezinye ii-apps. Ifestile yencoko yababini yokuqala ikuvumela ukuba uxelele i-app into oyifunayo. Tshintsha\in\iinketho ukuba ufuna njalo, emva koko ucofe Ok. Emva Khetha ulawulo olwahlukileyo Yenza i-thumbnail Khetha ikhesa Cima iphepha: Ifestile yencoko yababini: Ukuhlela kunye nochazo ngeMTPaint: Umyalezo wemposiso ovela kwi-imgur: Ifayile ayikho; ukutsiba ifayile ye-$ Ifayile igciniwe, Ilungele ukwenza inkqubo ekhethiweyo, umfanekiso wekhusi ugcinwe njenge: $SAVEDIR/$file_name.$file_ext Isikrini esiphelele Ukuba ikhethiwe, ifoto ikhutshelwa kwibhodi eqhotyoshwayo ukuze ukwazi ukuyincamathelisa ngokuthe ngqo
	 kwezinye ii-apps (ezifana neYahoo okanye i-imeyile ye-AOL bhala izikrini) okanye amaxwebhu
	 njengeLibreOffice Writer, njalo njalo, ngaphandle kokufuna ukuncamathisela. ifayile. Ukuba uvula isiqulathi seefayili kumphathi wefayile, unokucofa ekunene iifayile ukuze ubone
	 ukuba zeziphi iinketho eza kuzivumela. Uninzi lwabaphathi beefayile baya kukuvumela ukuba wongeze okanye
	 utshintshe iinketho zenkqubo ukuba azibonakali ngendlela ongathanda ngayo. Ukuba ubhalisa i IMGUR Client API Id, ungangenisa eyakho kwibhokisi
	 ebonelelweyo ngaphezulu kwexabiso elimiselweyo elinikiweyo. Ukuba uneengxaki nge-IMGUR
	 ayisebenzi rhoqo, zama ukubhalisa eyakho i-ID yoMxumi we-API. Kwimo yefestile, ukuba uneengxaki zokonakala kwesikrini (i-zzzFM okanye i-SpaceFM
	 i-desktop kunye ne-Fluxbox ingabonisa iingxaki), zama ukungakhethi imida yeefestile
	 ukubona ukuba iyanceda. Ukhetho lokubandakanya isalathisi semouse ngokwaso kweyakho
	 umfanekiso iyafumaneka kwimo yefestile. Bandakanya isalathisi se mouse Bandakanya umda wefestile Inokugcina umfanekiso wakho kwiindidi ezahlukeneyo zeefayile, kuphela kwezo zixhaswayo
	 yi-app esezantsi ethatha umfanekiso, iScrot, kunye neMTPaint, inkqubo esetyenziselwa uku
	 ukujonga, ukuchaza kunye nokuhlela iifoto zesikrini. Ingathatha umfanekiso wendawo ebonakalayo oyikhethayo ngekhesa yemouse,


	a ebonakalayo Ifestile, okanye isikrini sonke ekubambezelekeni. Inenketho yokwenza ubhontsi wepic yakho, kwaye yenza ubhontsi
	 emva kokuba wenze isichasiselo sakho sokuqala kunye nokuhlelwa ngeMTPaint. Kuvumelekile
	 ukwenza ubhontsi ngaphezulu kwe-100% yobungakanani boqobo. Iyakugqibeka ekudaleni nasekusebenziseni iScreenshots kwifolda yakho yasekhaya
	, kodwa ungayitshintsha kulawulo olwahlukileyo ukuba uyathanda. ILibreOffice ayifakwanga. Ukuvula Ummandla oza kubanjwa Gcina Isimo: Gcina kwaye uvule ngeMTPaint ivula zombini umfanekiso wekhusi kunye nobhontsi ukuba ubhontsi wenziwa
	. Umfanekiso wekhusi Iifoto zekhusi Ezinye ii-apps ezikwimenyu zithiywe ngokubanzi ngenxa yokuba eyona app eyakuthi
	 isebenze yeyangoku ekhethiweyo kwiSicelo esiKhethiweyo. I-pic iya kuthi emva koko ivuleke kwiMTPaint ukuze ujonge, uhlele, kwaye uchaze. Zonke
	 zeempawu zeMTPaint ziyafumaneka. I-Thumbnail yamaphepha ewebhu Ukongeza isiqendu esongezelelweyo kumfanekiso wakho, cofa nje i-icon ye-T kwaye ufake okubhaliweyo
	 kumhlaba onikiweyo. Ungalungisa ubungakanani befonti kunye neempawu ukuba uyafuna. Cofa i Cola Isiqendu iqhosha xa ugqibile. Emva koko ukhethe kwaye utsale okubhaliweyo ukuya
	 apho ufuna ukubekwa khona. Ukuba ayilungile, cofa u-Ctrl-z ukuze uhlehlise kwaye uzame
	 kwakhona. Ukuzoba umgca ngotolo ekupheleni ucofa i icon yoMlayini oLungileyo
	 onguMlawuli, kwaye emva koko yalathe apho umgca kufuneka uqale khona kwaye ucofe, utsale
	 apho elo candelo lomgca. kufuneka iphele. Yeka iqhosha lemouse ukuya
	 ekupheleni elo candelo lomgca. Ungongeza elinye icandelo ngokucofa kunye
	 ukutsala kwakhona. Emva kokuba icandelo lomgca lizotyiwe, ungacofa iqhosha elithi A
	 kwaye liyakuguqula isiphelo somgca sibe yintloko yotolo. Emva koko cofa iqhosha lika-Esc
	 ukuphelisa indlela yokuzoba yomgca. Ukulayisha kwi-IMGUR akuphumelelanga Kunyuswa i-URL kwi-IMGUR $file Kufakwe ifayile kwi IMGUR $file Xa ugqibile ukuhlela kunye nochazo, cofa i Gcina iqhosha kwaye emva koko Vala
	 iqhosha. Emva kokuba uphumile ubhontsi wakho uya kwenziwa ngokuzenzekelayo kwi-
	 egciniweyo umfanekiso wekhusi. Ifestile Ifestile KUFUNEKA ibonakale ngokupheleleyo Ukwanalo nokhetho lokurhoxisa okanye lokuphuma, ukuthatha enye
	 umfanekiso weskrini, okanye ukugcina ifayile kwaye uphume. Unokukhetha igama lesiseko seefayile ofuna zenziwe ngaphakathi kwenkqubo
	 imida kwaye iya ngokuzenzekelayo isimamva igama kunye nomhla wangoku
	 kunye nexesha lokuqinisekisa ukuba iya kuba yodwa. Ungayifakela ngokuzenzekelayo inqaku lombhalo kumfanekiso wakho wekhusi, ngokukhetha
	 ngoMhla wangoku:Ixesha elongezwe njengesimamva kumtya wakho wokubhaliweyo ngokunjalo.
	 Xa isiza kwiscreen seMTPaint, tsala ngokulula. umbhalo oya apho


wufuna khona kumfanekiso wekhusi. Ukuba uyanqweneleka, ifonti, ubungakanani, indawo kunye nombala,
	 njl. umfanekiso wekhusi 