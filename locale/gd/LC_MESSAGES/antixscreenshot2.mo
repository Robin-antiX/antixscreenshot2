��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  7     +  ;  �   g  �     �        �     �     �     �     �       )     "   F  2   i  �   �     '  #  3  8  W   +  �!  ]  �"  !   $  #   <$  '  `$  �   �%  �   &  �   	'  $   �'     �'     �'     (  |   (     �(     �(  �   �(  �   y)  #   #*  �  G*  �  ,  #   �.  #   �.  ,   /  �   >/     <0  .   D0  x   s0  �   �0  �  �1     �3            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Gaelic, Scottish (https://www.transifex.com/antix-linux-community-contributions/teams/121548/gd/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gd
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
 Gnìomh Uinneag gnìomh: Cuir Ceann-latha @ Ùine gus an teacsa a chomharrachadh Às deidh an dealbh agus an òrdag a chruthachadh, nochdaidh uinneag Gnìomh far an innis thu dha dè na gnìomhan a bu chòir a dhèanamh leis an dealbh agad. Aig a' mhullach seallaidh e
	 an deach an dealbh-sgrìn a shàbhaladh no nach deach, agus an t-slighe agus ainm an fhaidhle a thàinig às. Às deidh an dealbh a thogail, ma tha e den làn-sgrìn, thèid a lughdachadh 
	 gu 85% gus am bi e a’ freagairt air an uinneag deasachaidh agus gum bi e nas fhasa a dheasachadh. Aplacaid a thogas dealbh dhen sgrion agad, a leigeas leat





 ro-shealladh a dhèanamh air, a chomharrachadh is a dheasachadh, is a shàbhaladh, a bharrachd air a phasgadh a-steach no












 fosgladh le aplacaidean eile. Leigidh uinneag Còmhraidh tùsail dhut innse don aplacaid dè a tha thu ag iarraidh. Atharraich\in
a roghainnean ma dh'fheumas tu, agus an uairsin cliog air OK. Air ais Tagh eòlaire eadar-dhealaichte Cruthaich mion-dhealbh Taghadh Cursor Sguab às an duilleag: Uinneag còmhraidh: Deasachadh agus comharrachadh le MTPaint: Teachdaireachd mearachd bho imgur: Chan eil am faidhle ann; a' leum thairis air $file Faidhle air a shàbhaladh, Deiseil airson pròiseas taghte a dhèanamh, dealbh-sgrìn air a shàbhaladh mar: $SAVEDIR/$file_name.$file_ext Làn-sgrìn Ma thèid a thaghadh, thèid an dealbh a chopaigeadh chun a’ bhòrd bhidio gus an urrainn dhut a phasgadh gu dìreach ann an aplacaidean eile (leithid scrionaichean sgrìobhadh post-d Yahoo no AOL) no sgrìobhainnean
	 mar LibreOffice Writer, msaa, gun a bhith feumach air ceangal faidhle. Ma dh'fhosglas tu am pasgan ann am manaidsear fhaidhlichean, 's urrainn dhut briogadh deas air na faidhlichean gus faicinn
	 dè na roghainnean a cheadaicheas e. Leigidh a’ mhòr-chuid de mhanaidsearan fhaidhlichean leat
	 roghainnean a’ phrògraim a chur ris no atharrachadh mura nochd iad mar a thogras tu. Ma chlàras tu IMGUR Client API Id, faodaidh tu do chuid fhèin a chuir a-steach sa bhogsa
	 a chaidh a sholarachadh a’ dol thairis air an luach bunaiteach a chaidh a thoirt seachad. Ma tha duilgheadas agad le IMGUR
	 nach eil ag obair gu tric, feuch an clàraich thu an ID Client API agad fhèin. Ann am modh uinneig, ma tha trioblaidean coirbeachd sgrion agad (faodaidh zzzFM no SpaceFM
	 desktop agus Fluxbox duilgheadasan a nochdadh), feuch gun tagh thu crìochan uinneig
	 gus faicinn a bheil e na chuideachadh. Tha roghainn airson cursair na luchaige fhèin a chur a-steach san dealbh agad
	 cuideachd ri fhaighinn ann am modh na h-uinneige. Cuir a-steach cursair na luchaige Cuir a-steach crìoch na h-uinneige Faodaidh e an dealbh agad a shàbhaladh gu diofar sheòrsan fhaidhlichean, cuingichte don fheadhainn a tha a’ faighinn taic
	 leis an aplacaid togail dhealbhan bunaiteach, Scrot, agus MTPaint, am prògram a chleachdar gus
	 na seallaidhean-sgrìn fhaicinn, a chomharrachadh agus a dheasachadh. Gabhaidh e dealbh de raon faicsinneach a thaghas tu le cursair na luchaige,
	a uinneag fhaicsinneach, no an sgrìn gu lèir air dàil. Tha roghainn aige òrdag a chruthachadh airson do dhealbh, agus cruthaichidh e an òrd
	 às deidh dhut a’ chiad sgrìobhadh agus deasachadh agad a dhèanamh le MTPaint. Tha e ceadaichte
	 òrdag a chruthachadh nas motha na 100% den mheud tùsail. Bidh e bunaiteach a bhith a’ cruthachadh agus a’ cleachdadh pasgan Screenshots san eòlaire dachaigh
	 agad, ach ’s urrainn dhut atharrachadh gu eòlaire eile ma thogras tu. Chan eil LibreOffice air a stàladh. Fosgladh Sgìre airson a ghlacadh Sàbhail Inbhe: Sàbhail is fosgail le MTPaint a’ fosgladh an dà chuid an dealbh-sgrìn agus an òrdag ma chaidh òrdag a chruthachadh
	. Glacadh-sgrìn Glacaidhean-sgrìn Tha cuid de na h-aplacaidean air a’ chlàr air an ainmeachadh gu coitcheann a chionn ’s gur e an dearbh aplacaid a ruitheas
	 an aplacaid àbhaisteach a thagh thu ann an Tagraidhean Roghaichte. Fosglaidh an dealbh an uairsin ann am MTPaint gus an urrainn dhut ro-shealladh, deasachadh agus notaichean a chuir ris. Tha a h-uile 
	 de fheartan MTPaint rim faighinn. Mion-dhealbh airson duilleagan-lìn Gus teacsa a bharrachd a chuir ris an dealbh agad, dìreach cliog air an ìomhaigh T agus cuir a-steach an teacsa
	 san raon a chaidh a sholarachadh. 'S urrainn dhut meud cruth-clò agus buadhan atharrachadh ma thogras tu. Cliog air a’ phutan Paste Text nuair a bhios tu deiseil. An uairsin tagh is slaod an teacsa gu
	 far a bheil thu ag iarraidh a shuidheachadh. Mura h-eil e ceart, brùth Ctrl-z gus a dhì-dhèanamh is feuch
	 a-rithist. Gus loidhne a tharraing le saighead aig an deireadh cliogaidh tu air an ìomhaigheag Loidhne Dhìreach a tha
	 na Riaghladair, agus an uairsin comharraich far am bu chòir don loidhne tòiseachadh is cliogadh, an uairsin slaod
	 gu far a bheil an earrann sin den loidhne bu chòir crìochnachadh. Leig às putan na luchaige gus
	 crìoch a chur air an earrann sin den loidhne. 'S urrainn dhut earrann eile a chur ris le bhith a' briogadh is
	 a' slaodadh a-rithist. Às deidh don earrann den loidhne a bhith air a tharraing, faodaidh tu an iuchair A a bhrùthadh
	 agus tionndaidhidh e deireadh na loidhne gu ceann saighead. An uairsin brùth air an iuchair Esc
	 gus am modh tarraing loidhne crìochnachaidh. Dh'fhàillig luchdadh suas gu IMGUR A' luchdadh suas URL gu IMGUR $file A’ luchdachadh suas faidhle gu IMGUR $file Nuair a bhios tu deiseil a’ deasachadh agus a’ comharrachadh, cliog air a’ phutan Sàbhail agus an uairsin air a’ phutan Dùin
	. Às deidh dhut falbh thèid d’ òrdag a ghineadh gu fèin-ghluasadach on
	 glacadh-sgrìn a chaidh a shàbhaladh. Uinneag FEUMAIDH an uinneag a bhith gu tur ri fhaicinn Tha roghainn agad cuideachd dìreach cuir dheth no falbh, glacadh-sgrìn eile
	, no am faidhle a shàbhaladh agus falbh. 'S urrainn dhut bun-ainm nam faidhle a tha thu ag iarraidh a chruthachadh taobh a-staigh crìochan an t-siostaim
	 a thaghadh agus cuiridh e an t-ainm ris a' cheann-latha làithreach
	 agus an ùine gus a bhith cinnteach gum bi e gun samhail. Faodaidh tu a bhith air nòta teacsa a chuir ris an dealbh-sgrìn agad gu fèin-ghluasadach, gu roghnach 
	 leis an Ceann-latha gnàthach: Ùine air a chur ris mar iar-leasachan ris an t-sreang teacsa agad cuideachd.
	 Nuair a thig e suas air scrion MTPaint, dìreach slaod an teacsa far a bheil thu
	 ga iarraidh air an ath-sgrìn. Ma thogras tu, faodar an cruth-clò, meud, suidheachadh is dath,
	 msaa a thaghadh ann an còmhradh MTPaint Paste Text. dealbh-sgrìn 