��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �    
   �     �  4   �  �  �  �   �  /  �    �  
   �  .   �           6  !   P     r  D   �  *   �  8      �   :      �   z  !  �  "  �  Y$  5  &  (   G(  .   p(  �  �(  �   .*  z  +  
  �,  *   �-     �-  '   �-     �-  �   .     �.     �.  9  �.  �   !0  /    1  �  01  �  �3  1   �7  .   �7  -   8  R  B8     �9  >   �9  �   �9  _  �:  g  �;     `>            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Kyrgyz (https://www.transifex.com/antix-linux-community-contributions/teams/121548/ky/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ky
Plural-Forms: nplurals=1; plural=0;
 Акция Аракет терезеси: Белги текстине Date@Time кошуңуз Сүрөт жана баш бармак түзүлгөндөн кийин, »Аракет« терезеси ачылат, анда сиз сүрөтүңүзгө кандай аракеттерди жасоо керектигин айтасыз. Жогору жагында скриншот
	 сакталганбы же сакталбаганы, натыйжада жол менен файлдын аталышы көрсөтүлөт. Сүрөт тартылгандан кийин, эгер ал толук экранда болсо, ал 85% чейин кичирейтилип, түзөтүү терезесине туура келет жана түзөтүү оңой болот. Экраныңыздын сүрөтүн тарткан колдонмо, аны алдын ала көрүү, аннотациялоо жана түзөтүү, сактоо, ошондой эле аны башка колдонмолорго чаптоо же
	
	тоого мүмкүндүк берет. Алгачкы диалог терезеси колдонмого сиз каалаган нерсени айтууга мүмкүндүк берет. Керек болсо, параметрлерди өзгөртүп, андан кийин OK чыкылдатыңыз. Артка Башка каталогду тандаңыз Эскиз түзүү Курсор тандоо Баракты жок кылуу: Диалог терезеси: MTPaint менен түзөтүү жана аннотациялоо: imgurдан ката билдирүүсү: Файл жок; $файл өткөрүп жиберүү Файл сакталды, тандалган процессти аткарууга даяр, скриншот төмөнкүдөй сакталды: $SAVEDIR/$file_name.$file_ext Толук экран Эгер тандалган болсо, сүрөт алмашуу буферине көчүрүлөт, андыктан сиз аны түз
	 башка колдонмолорго (мисалы, Yahoo же AOL почта жазуу экрандары) же LibreOffice Writer сыяктуу документтерге
	, тиркөөнүн кереги жок, ж.б. файл. Эгер папканы файл башкаргычында ачсаңыз, файлдарды оң баскыч менен чыкылдатып, ал кандай параметрлерге жол берерин
	 көрө аласыз. Көпчүлүк файл менеджерлери программанын параметрлери сиз каалагандай көрүнбөсө, аларды кошууга же өзгөртүүгө уруксат беришет. Эгерде сиз IMGUR Client API Id'ди каттасаңыз, берилген демейки маанини жокко чыгарып, 
	 берилген кутуга өзүңүздүкүн киргизсеңиз болот. IMGUR






































 иштебей турган көйгөйлөр бар болсо, анда өз Client API идентификаторуңузду каттатып көрүңүз. Терезе режиминде, экраныңыздын бузулушуна байланыштуу көйгөйлөрүңүз болсо (zzzFM же SpaceFM
	 иш тактасы жана Fluxbox көйгөйлөрдү жаратышы мүмкүн), ал жардам береби же жокпу, көрүү үчүн Терезе чектерин тандоодон алып көрүңүз
	. Чычкан курсорунун өзүн сиздин
	 сүрөтүңүзгө кошуу опциясы Терезе режиминде да жеткиликтүү. Чычкан курсорун кошуу Терезенин чектерин кошуу Ал скриншотторду көрүү, аннотациялоо жана түзөтүү үчүн колдонулган негизги сүрөт тартуу колдонмосу, Scrot жана MTPaint программасы тарабынан колдоого алынгандар менен чектелип, ар кандай файлдардын түрлөрүнө сактай алат. Ал чычкан курсору менен тандалган көрүнгөн аймактын,
	a көрүнүүчү терезенин же кечигүү менен бүт экрандын сүрөтүн тарта алат. Анын сүрөтүңүз үчүн баш бармагын түзүү мүмкүнчүлүгү бар жана MTPaint менен баштапкы аннотацияңызды жана түзөтүүнү жасагандан кийин ал бармакты жаратат. Баш бармакты баштапкы өлчөмдөн 100% түзүүгө жол берилет
	. Ал үйүңүздөгү
	 каталогуңузда Скриншоттор папкасын түзүү жана колдонуу үчүн демейки болот, бирок кааласаңыз, аны башка каталогго өзгөртө аласыз. LibreOffice орнотулган эмес. Ачылышы басып алуу үчүн аймак Статусту сактоо: Сактоо жана MTPaint менен ачуу скриншотту да, бармакты да ачат, эгерде баш бармак түзүлбөсө
	. Скриншот Скриншоттор Менюдагы кээ бир колдонмолор жалпы аталыш менен аталат, анткени иштетилбей турган чыныгы колдонмо Артыкчылыктуу колдонмолордо тандалган учурдагы демейки болуп саналат. Сүрөт андан кийин алдын ала көрүү, түзөтүү жана аннотациялоо үчүн MTPaintте ачылат. MTPaintтин бардык
	 функциялары жеткиликтүү. Веб баракчалар үчүн эскиз Сүрөтүңүзгө кошумча Текст кошуу үчүн, жөн гана T белгисин чыкылдатып, берилген талаага текстти
	 киргизиңиз. Кааласаңыз, шрифттин өлчөмүн жана атрибуттарын тууралай аласыз. Бүткөндөн кийин Текстти коюу баскычын басыңыз. Андан кийин текстти тандап, аны жайгаштыргыңыз келген жерге сүйрөңүз. Эгер ал туура эмес болсо, жокко чыгаруу үчүн Ctrl-z баскычтарын басыңыз жана кайра
	 аракет кылыңыз. Аягына жебе менен сызык тартуу үчүн, сиз 
	 Сызгыч болгон Түз сызык сүрөтчөсүн чыкылдатып, андан кийин сызык баштала турган жерди көрсөтүп, чыкылдатыңыз, андан кийин сызыктын ошол бөлүгүнө сүйрөңүз
	 бүтүшү керек. Чычкан баскычын коё бериңиз, сызыктын ал бөлүгүн
	 бүтүрүңүз. Дагы бир бөлүмдү чыкылдатып,
	 кайра сүйрөө менен кошо аласыз. Сызыктын кесилиши тартылгандан кийин, сиз A баскычын бассаңыз болот
	 жана ал сызыктын учуна жебенин учуна айланат. Андан кийин сызыктарды тартуу режимин аяктоо үчүн Esc
	 баскычын басыңыз. IMGURга жүктөө ишке ашкан жок URL IMGUR $файлына жүктөлүүдө Файл IMGUR $fileге жүктөлүүдө Түзөтүү жана аннотациялоо аяктагандан кийин, Сактоо баскычын, андан кийин Жабу
	 баскычын басыңыз. Чыкканыңыздан кийин баш бармагыңыз сакталган скриншоттон
	 автоматтык түрдө түзүлөт. Терезе Терезе толук көрүнүп турушу КЕРЕК Сизде жөн эле жокко чыгаруу же чыгуу, башка
	 скриншот алуу же файлды сактап, чыгуу мүмкүнчүлүгү бар. Сиз түзүүнү каалаган файлдардын базалык атын системанын
	 чектөөлөрүндө тандай аласыз жана ал уникалдуу болушуна ынануу үчүн аталышты автоматтык түрдө учурдагы дата жана убакыт менен кошот. Сиз аны скриншотуңузга автоматтык түрдө текст эскертүүсүн кошо аласыз, кааласаңыз
	 учурдагы Дата:Убакыт текст сапыңызга суффикс катары кошулган.
	 Ал MTPaint экранында келгенде, жөн гана сүйрөңүз. текстти скриншоттун сиз каалабаган жерине жөнөтүңүз. Кааласаңыз, MTPaint Paste Text диалогунан шрифти, өлчөмүн, ордун жана түсүн, 
	 ж.б. тандаса болот. скриншот 