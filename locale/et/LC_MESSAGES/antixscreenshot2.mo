��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  ,   �  �   �  �   �  �   W  �        �     �     �     �     �     �  /   �       (   !  {   J     �  �   �  �   �  �   �    �      �!     �!  �   �!  w   �"  �   $#  �   �#     r$     �$     �$     �$  c   �$     &%     2%  �   @%  �   �%     l&  �  �&  �  (  #   �)  "   
*  "   -*  �   P*     +  #   +  p   4+  �   �+  w  Y,     �-            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/et/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
 Tegevus Toiminguaken: Teksti märkimiseks lisage kuupäev@kellaaeg Pärast pildi ja pöidla loomist avaneb toiminguaken, kus saate
	 öelda, milliseid toiminguid oma pildiga teha. Ülaosas kuvatakse see, kas
	 ekraanipilt on salvestatud või mitte, ning sellest tulenev tee ja failinimi. Kui pilt on täisekraanil, vähendatakse seda pärast pildistamist
	 85%-ni, et see mahuks redigeerimisaknasse ja seda oleks lihtsam redigeerida. Rakendus, mis teeb teie ekraanipildi, võimaldab


 seda eelvaadata, lisada märkusi ja redigeerida ning salvestada, samuti kleepida või
\avada teistesse rakendustesse. Esialgne dialoogiaken võimaldab teil rakendusele öelda, mida soovite. Muutke vajaduse korral suvandeid ja klõpsake siis nuppu OK. tagasi Valige erinev kataloog Loo pisipilt Kursori valimine Kustuta leht: Dialoogiaken: Redigeerimine ja märkuste tegemine MTPaintiga: Veateade imgurilt: Faili pole olemas; $faili vahelejätmine Fail salvestatud, valitud protsessi teostamiseks valmis, ekraanipilt salvestatud järgmiselt: $SAVEDIR/$file_name.$file_ext Täisekraan Kui see on valitud, kopeeritakse pilt lõikepuhvrisse, et saaksite selle otse kleepida
	 teistesse rakendustesse (nt Yahoo või AOL-i e-kirjade koostamise ekraanid) või dokumentidesse,
	 nagu LibreOffice Writer jne, ilma et peaksite lisama. fail. Kui avate kausta failihalduris, saate failidel paremklõpsata, et näha
	, milliseid valikuid see võimaldab. Enamik failihaldureid lubab teil programmisuvandeid lisada või

 mitte muuta, kui need ei kuvata nii, nagu soovite. Kui registreerite IMGUR Client API ID, saate sisestada antud väljale
	 enda ID, mis alistab antud vaikeväärtuse. Kui teil on probleeme IMGUR-iga,

 ei tööta sageli, proovige registreerida oma kliendi API ID. Kui teil on aknarežiimis ekraani riknemisega probleeme (zzzFM või SpaceFM
	 desktop ja Fluxbox võivad probleeme tekitada), proovige tühistada akna ääriste valik
	, et näha, kas see aitab. Võimalus lisada oma pildile
	 hiirekursor ise on saadaval ka aknarežiimis. Kaasake hiirekursor Kaasake akna ääris See võib salvestada teie pildi erinevat tüüpi failidesse, piirdudes nendega, mida
	 toetavad aluseks olev pildistamisrakendus, Scrot ja MTPaint – programm, mida
	 kasutatakse ekraanipiltide vaatamiseks, märkimiseks ja redigeerimiseks. See võib viivitusega pildistada nähtavast alast, mille valite hiirekursoriga,
	a nähtavaks aknaks või kogu ekraani. Sellel on võimalus luua teie pildile pöidla ja see loob pöidla
	 pärast esialgse märkuste lisamist ja MTPaintiga redigeerimist. Lubatud on
	 luua pöidla, mis ületab 100% algsest suurusest. Vaikimisi luuakse ja kasutatakse kausta Screenshots teie kodukataloogis
	, kuid soovi korral saate selle muuta mõneks muuks kataloogiks. LibreOffice pole installitud. Avamine Piirkond jäädvustamiseks Salvesta olek: MTPaintiga salvestamine ja avamine avab nii ekraanipildi kui ka pöidla, kui pöidlast
\ ei loodud. Ekraanipilt Ekraanipildid Mõned menüüs olevad rakendused on nimed üldiselt, kuna tegelik rakendus, mis
\ ei tööta, on praegu eelistatud rakendustes valitud vaikeseade. Seejärel avaneb pilt rakenduses MTPaint, et saaksite selle eelvaate, redigeerimise ja märkuste lisamiseks. Kõik
	 MTPainti funktsioonid on saadaval. Veebilehtede pisipilt Pildile täiendava teksti lisamiseks klõpsake lihtsalt ikoonil T ja sisestage tekst
	 ettenähtud väljale. Saate kohandada fondi suurust ja atribuute, kui te
\ ei soovi. Kui olete lõpetanud, klõpsake nuppu Kleebi tekst. Seejärel valige ja lohistage tekst sinna,
	 kuhu soovite selle paigutada. Kui see pole õige, vajutage tagasivõtmiseks klahvikombinatsiooni Ctrl-z ja proovige
	 uuesti. Lõpus noolega joone joonistamiseks klõpsa sirgjoone ikooni,
	 on joonlaud, seejärel osuta sellele, kust joon peaks algama ja klõpsa, seejärel lohista
	 sellesse jooneosasse peaks lõppema. Selle reaosa lõpetamiseks
	 vabastage hiirenupp. Saate lisada veel ühe jaotise, klõpsates ja
	 uuesti lohistades. Pärast joonelõigu joonistamist võite vajutada klahvi A
	 ja see muudab rea lõpu nooleotsaks. Seejärel vajutage joonestusrežiimi lõpetamiseks klahvi Esc
	. IMGUR-i üleslaadimine ebaõnnestus URL-i üleslaadimine faili IMGUR $ Faili üleslaadimine IMGUR $-faili Kui olete redigeerimise ja märkuste lisamise lõpetanud, klõpsake nuppu Salvesta ja seejärel nuppu Sule
	. Pärast väljumist luuakse salvestatud ekraanipildist
	 automaatselt pöial. Aken Aken PEAB olema täielikult nähtav Teil on ka võimalus lihtsalt tühistada või väljuda, teha uus
	 ekraanipilt või fail salvestada ja väljuda. Saate valida süsteemi
	 piirangute piires loodavatele failidele põhinime ja see lisab nimele automaatselt praeguse kuupäeva
	 ja kellaaega, et olla kindel, et see on kordumatu. Saate lasta sellel lisada oma ekraanipildile automaatselt tekstimärkuse, soovi korral
	 kui praegune kuupäev: kellaaeg on lisatud ka teie tekstistringi järelliitena.
	 Kui see kuvatakse MTPaint ekraanil, lohistage lihtsalt tekst sinna, kuhu te
\ ei soovi ekraanipildil. Soovi korral saab MTPaint Teksti kleepimise dialoogis valida fondi, suuruse, asukoha ja värvi,
	 jne. ekraanipilt 