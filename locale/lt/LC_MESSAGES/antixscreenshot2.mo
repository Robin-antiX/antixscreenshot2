��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  %       *     3  1   C  �   u  �   U  �   �  z   �     �                7     N     b  .   r     �  +   �  x   �     e  �   w    r  �   �   =  d!     �"     �"  �   �"  �   �#  �   E$  �   %     �%  
   �%     �%     �%  f   &     y&     �&  �   �&  �   J'     �'  �  �'  H  k)     �+     �+  "   �+  �   ,     �,  &   �,  r   �,  �   m-  �  U.     �/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 Veiksmas Veiksmo langas: Pridėkite Date@Time, kad užrašytumėte tekstą Sukūrus paveikslėlį ir nykštį, pasirodo veiksmų langas, kuriame nurodote
	, kokius veiksmus atlikti su nuotrauka. Viršuje bus rodoma, ar
	 ekrano kopija buvo išsaugota, ar ne, ir gautas kelias bei failo pavadinimas. Jei nuotrauka bus padaryta viso ekrano režimu, ji bus sumažinta
	 iki 85 %, kad tilptų redagavimo lange ir būtų lengviau ją redaguoti. Programa, kuri nufotografuoja ekraną, leidžia
\peržiūrėti, komentuoti, redaguoti ir išsaugoti, taip pat įklijuoti arba
\atidaryti su kitomis programomis. Pradiniame dialogo lange galite pasakyti programai, ko norite. Jei reikia, pakeiskite parinktis, tada spustelėkite Gerai. Atgal Pasirinkite kitą katalogą Sukurkite miniatiūrą Žymeklio pasirinkimas Ištrinti puslapį: Dialogo langas: Redagavimas ir komentavimas naudojant MTPaint: Klaidos pranešimas iš imgur: Failas neegzistuoja; praleidžiamas $failas Failas išsaugotas, paruošta atlikti pasirinktą procesą, ekrano kopija išsaugota kaip: $SAVEDIR/$file_name.$file_ext Per visą ekraną Jei pasirinkta, paveikslėlis nukopijuojamas į mainų sritį, kad galėtumėte jį tiesiogiai
	 įklijuoti į kitas programas (pvz., Yahoo ar AOL el. laiškų kūrimo ekranus) arba dokumentus,
	 kaip LibreOffice Writer ir pan., neprisegdami. failas. Jei atidarote aplanką failų tvarkyklėje, galite dešiniuoju pelės mygtuku spustelėti failus ir pamatyti,
	 kokias parinktis tai leis. Daugelis failų tvarkyklių leis jums pridėti arba

 nekeisti programos parinkčių, jei jos pasirodys ne taip, kaip norėtumėte. Jei užregistruojate IMGUR kliento API ID, pateiktame laukelyje
	 galite įvesti savo, nepaisydami numatytosios reikšmės. Jei kyla problemų dėl IMGUR

 neveikiančio dažnai, pabandykite užregistruoti savo kliento API ID. Jei naudojate lango režimą, turite ekrano sugadinimo problemų (žzzFM arba SpaceFM
	 darbalaukis ir Fluxbox gali sukelti problemų), pabandykite panaikinti lango kraštinių pasirinkimą
	, kad sužinotumėte, ar tai padeda. Parinktis įtraukti patį pelės žymeklį į nuotrauką
	 taip pat galima lango režimu. Įtraukti pelės žymeklį Įtraukti lango kraštą Jis gali išsaugoti jūsų paveikslėlį į įvairių tipų failus, tik tuos, kuriuos palaiko
	 pagrindinė fotografavimo programa, Scrot ir MTPaint – programa, naudojama

 peržiūrėti, komentuoti ir redaguoti ekrano kopijas. Su uždelsimu galima nufotografuoti matomą sritį, kurią pasirinkote naudodami pelės žymeklį,
	a matomą langą arba visą ekraną. Ji turi galimybę sukurti nykštį jūsų nuotraukai ir sukuria nykštį
	, kai atliksite pradinį komentarą ir redaguosite naudodami MTPaint. Leidžiama
	 sukurti nykštį, viršijantį 100 % pradinio dydžio. Pagal numatytuosius nustatymus bus sukurtas ir naudojamas ekrano kopijų aplankas jūsų namų
	 kataloge, tačiau, jei norite, galite pakeisti jį į kitą katalogą. LibreOffice neįdiegtas. Atidarymas Užfiksuotinas regionas Išsaugoti būseną: Įrašyti ir atidaryti naudojant MTPaint atidaro ir ekrano kopiją, ir nykštį, jei

 nebuvo sukurta. Ekrano kopija Ekrano nuotraukos Kai kurios meniu esančios programos pavadintos bendrai, nes tikroji programa, kuri

epaleidžiama, yra dabartinė numatytoji, pasirinkta skiltyje „Preferred Applications“. Tada paveikslėlis bus atidarytas MTPaint, kad galėtumėte peržiūrėti, redaguoti ir komentuoti. Galimos visos
	 MTPaint funkcijos. Tinklalapių miniatiūra Norėdami pridėti papildomo teksto prie nuotraukos, tiesiog spustelėkite piktogramą T ir įveskite tekstą
	 pateiktame lauke. Jei
\ nenorite, galite koreguoti šrifto dydį ir atributus. Baigę spustelėkite mygtuką Įklijuoti tekstą. Tada pasirinkite ir vilkite tekstą į
	 ten, kur norite. Jei tai neteisinga, paspauskite Ctrl-z, kad anuliuotumėte ir bandykite
	 dar kartą. Norėdami nubrėžti liniją su rodykle pabaigoje, spustelėkite tiesios linijos piktogramą, kuri
	 yra liniuotė, tada nukreipkite žymiklį į vietą, kur linija turėtų prasidėti ir spustelėkite, tada vilkite
	 ten, kur ta linijos dalis turėtų baigtis. Atleiskite pelės mygtuką, kad

ebaigtumėte tos linijos dalies. Galite pridėti kitą skyrių spustelėdami ir

 nevilkdami dar kartą. Nubrėžę linijos atkarpą, galite paspausti klavišą A
	 ir eilutės pabaiga pavers rodyklės smaigaliu. Tada paspauskite klavišą Esc
	, kad baigtumėte linijos piešimo režimą. Įkelti į IMGUR nepavyko Įkeliamas URL į IMGUR $failą Failas įkeliamas į IMGUR $failą Baigę redaguoti ir komentuoti, spustelėkite mygtuką Išsaugoti, tada – mygtuką Uždaryti
	. Kai išeisite, nykštis bus automatiškai sugeneruotas iš
	 išsaugotos ekrano kopijos. Langas Langas PRIVALO būti visiškai matomas Taip pat galite tiesiog atšaukti arba išeiti, padaryti kitą
	 ekrano kopiją arba išsaugoti failą ir išeiti. Galite pasirinkti pagrindinį failų, kuriuos norite sukurti, pavadinimą, atsižvelgdami į sistemos
	 apribojimus, ir jis automatiškai pridės pavadinimą su dabartine data
	 ir laiku, kad įsitikintumėte, jog jis bus unikalus. Galite nustatyti, kad jis automatiškai pridėtų teksto pastabą prie ekrano kopijos, pasirinktinai
	 su dabartine data:Laikas taip pat pridėta prie teksto eilutės priesaga.
	 Kai jis pasirodo MTPaint ekrane, tiesiog vilkite tekstą į ten, kur

 norite, kad jis būtų ekrano kopijoje. Jei pageidaujate, dialogo lange MTPaint Paste Text galima pasirinkti šriftą, dydį, padėtį ir spalvą,
	 ir kt. ekrano kopija 