��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �  =   �  P   �  K  B  5  �  k  �    0     K   9   X   -   �   -   �      �   +   !  i   .!  O   �!  .   �!  �   "     �"  �  #    �$  �  '  �  �(  ?   S+  *   �+  �  �+  g  �-  �  /  I  1  3   d2     �2  -   �2  "   �2  �   �2     �3     �3  _  4  +  v5  H   �6  ]  �6  y  I:  I   �>  D   ?  K   R?  �  �?     }A  W   �A  �   �A  �  �B  E  �D     H            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Thai (https://www.transifex.com/antix-linux-community-contributions/teams/121548/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
 การกระทำ หน้าต่างการดำเนินการ: เพิ่ม Date@Time เพื่อบันทึกข้อความ หลังจากสร้างรูปและนิ้วหัวแม่มือแล้ว หน้าต่างการดำเนินการจะปรากฏขึ้นเพื่อให้คุณบอก
	ว่าต้องทำอะไรกับรูปของคุณ ที่ด้านบนจะแสดงว่า
	 ภาพหน้าจอได้รับการบันทึกหรือไม่ รวมทั้งเส้นทางที่เป็นผลลัพธ์และชื่อไฟล์ หลังจากถ่ายภาพแล้ว หากเป็นภาพเต็มหน้าจอ จะย่อขนาดลง
	เป็น 85% เพื่อให้พอดีกับหน้าต่างแก้ไขและแก้ไขได้ง่ายขึ้น แอปที่ถ่ายภาพหน้าจอของคุณ ช่วยให้คุณ
	ดูตัวอย่าง ใส่คำอธิบายประกอบและแก้ไข และบันทึก รวมถึงวางลงในหรือ
	เปิดร่วมกับแอปอื่นๆ ได้ หน้าต่างโต้ตอบเริ่มต้นช่วยให้คุณบอกแอปว่าต้องการอะไร เปลี่ยน\in\ตัวเลือกหากต้องการ จากนั้นคลิกตกลง กลับ เลือกไดเร็กทอรีอื่น สร้างภาพขนาดย่อ เลือกเคอร์เซอร์ ลบหน้า: หน้าต่างโต้ตอบ: การแก้ไขและใส่คำอธิบายประกอบด้วย MTPaint: ข้อความแสดงข้อผิดพลาดจาก imgur: ไม่มีไฟล์ ข้าม $file บันทึกไฟล์แล้ว พร้อมดำเนินการตามขั้นตอนที่เลือก บันทึกภาพหน้าจอเป็น: $SAVEDIR/$file_name.$file_ext เต็มจอ หากเลือก รูปภาพจะถูกคัดลอกไปยังคลิปบอร์ด คุณจึงสามารถวางได้โดยตรง
	 ลงในแอปอื่นๆ (เช่น หน้าจอเขียนอีเมลของ Yahoo หรือ AOL) หรือเอกสาร
	 เช่น LibreOffice Writer เป็นต้น โดยไม่ต้องแนบ ไฟล์. หากคุณเปิดโฟลเดอร์ในตัวจัดการไฟล์ คุณสามารถคลิกขวาที่ไฟล์เพื่อดู
	ตัวเลือกที่จะอนุญาต ตัวจัดการไฟล์ส่วนใหญ่จะอนุญาตให้คุณเพิ่มหรือ
	เปลี่ยนตัวเลือกโปรแกรมได้ หากไม่ปรากฏตามที่คุณต้องการ หากคุณลงทะเบียน IMGUR Client API Id คุณสามารถป้อนรหัสของคุณเองลงในช่อง
	 ที่ให้มาแทนที่ค่าเริ่มต้นที่ให้ไว้ หากคุณมีปัญหากับ IMGUR
	 ทำงานไม่บ่อย ให้ลองลงทะเบียน Client API Id ของคุณเอง ในโหมด Window หากคุณมีปัญหาหน้าจอเสียหาย (เดสก์ท็อป zzzFM หรือ SpaceFM
	 และ Fluxbox สามารถนำเสนอปัญหาได้) ให้ลองยกเลิกการเลือกขอบหน้าต่าง
	 เพื่อดูว่าจะช่วยได้หรือไม่ ตัวเลือกในการรวมเคอร์เซอร์ของเมาส์ไว้ในรูปภาพของคุณ
	 ก็มีให้ในโหมดหน้าต่างด้วย รวมเคอร์เซอร์ของเมาส์ รวมขอบหน้าต่าง มันสามารถบันทึกรูปภาพของคุณไปยังไฟล์ประเภทต่างๆ ได้ จำกัดเฉพาะไฟล์ที่รองรับ
	 โดยแอพถ่ายภาพพื้นฐาน, Scrot และ MTPaint ซึ่งเป็นโปรแกรมที่ใช้ในการดู
	 ใส่คำอธิบายประกอบ และแก้ไขภาพหน้าจอ มันสามารถถ่ายภาพของพื้นที่ที่มองเห็นได้ที่คุณเลือกด้วยเคอร์เซอร์ของเมาส์
	a หน้าต่างที่มองเห็นได้ หรือทั้งหน้าจอเมื่อดีเลย์ มีตัวเลือกในการสร้างนิ้วหัวแม่มือสำหรับรูปภาพของคุณ และสร้างนิ้วหัวแม่มือ
	 หลังจากที่คุณทำคำอธิบายประกอบเบื้องต้นและแก้ไขด้วย MTPaint อนุญาตให้
	สร้างนิ้วโป้งเกิน 100% ของขนาดดั้งเดิม ค่าเริ่มต้นคือการสร้างและใช้โฟลเดอร์สกรีนช็อตในไดเร็กทอรี home
	 แต่คุณสามารถเปลี่ยนเป็นไดเร็กทอรีอื่นได้หากต้องการ ไม่ได้ติดตั้ง LibreOffice เปิด ภูมิภาคที่จะจับ บันทึกสถานะ: บันทึกและเปิดด้วย MTPaint จะเปิดทั้งภาพหน้าจอและนิ้วหัวแม่มือ หาก
	สร้างนิ้วหัวแม่มือ ภาพหน้าจอ ภาพหน้าจอ แอปบางตัวในเมนูมีชื่อแบบทั่วไป เนื่องจากแอปจริงที่จะ
	เรียกใช้เป็นค่าเริ่มต้นปัจจุบันที่เลือกไว้ในแอปพลิเคชันที่ต้องการ รูปภาพจะเปิดขึ้นใน MTPaint เพื่อให้คุณดูตัวอย่าง แก้ไข และใส่คำอธิบายประกอบ ฟีเจอร์ทั้งหมดของ MTPaint
	 พร้อมใช้งาน รูปขนาดย่อสำหรับหน้าเว็บ หากต้องการเพิ่มข้อความให้กับรูปภาพของคุณ เพียงคลิกไอคอน T และป้อนข้อความ
	 ลงในช่องที่ให้ไว้ คุณสามารถปรับขนาดแบบอักษรและแอตทริบิวต์ได้หากต้องการ คลิกปุ่มวางข้อความเมื่อเสร็จแล้ว จากนั้นเลือกและลากข้อความไปยัง
	ตำแหน่งที่คุณต้องการให้อยู่ในตำแหน่ง หากไม่ถูกต้อง ให้กด Ctrl-z เพื่อเลิกทำแล้วลอง
	อีกครั้ง ในการวาดเส้นที่มีลูกศรต่อท้าย คุณคลิกไอคอน Straight Line ซึ่ง
	 คือไม้บรรทัด จากนั้นชี้ไปที่ตำแหน่งที่เส้นควรเริ่มต้นและคลิก จากนั้นลาก
	 ไปยังตำแหน่งส่วนนั้นของเส้น ควรจะจบ ปล่อยปุ่มเมาส์เพื่อ
	 จบส่วนนั้นของบรรทัด คุณสามารถเพิ่มส่วนอื่นได้โดยคลิกแล้ว
	ลากอีกครั้ง หลังจากที่วาดส่วนของเส้นแล้ว คุณสามารถกดแป้น A
	 แล้วปุ่มจะเปลี่ยนจุดสิ้นสุดของบรรทัดให้เป็นหัวลูกศร จากนั้นกดปุ่ม Esc
	 เพื่อสิ้นสุดโหมดการวาดเส้น การอัปโหลดไปยัง IMGUR ล้มเหลว กำลังอัปโหลด URL ไปยัง IMGUR $file กำลังอัปโหลดไฟล์ไปที่ IMGUR $file เมื่อแก้ไขและใส่คำอธิบายประกอบเสร็จแล้ว ให้คลิกปุ่มบันทึก จากนั้นคลิกปุ่มปิด
	 หลังจากที่คุณออกจากนิ้วโป้งของคุณจะถูกสร้างขึ้นโดยอัตโนมัติจาก
	ภาพหน้าจอที่บันทึกไว้ หน้าต่าง หน้าต่างต้องมองเห็นได้ทั้งหมด คุณยังมีตัวเลือกในการยกเลิกหรือออก ถ่ายภาพหน้าจออื่น
	 หรือบันทึกไฟล์และออก คุณสามารถเลือกชื่อฐานของไฟล์ที่คุณต้องการสร้างภายในข้อจำกัดของระบบ
	 และชื่อนี้จะต่อท้ายชื่อด้วยวันที่และเวลาปัจจุบัน
	 และเวลาปัจจุบันโดยอัตโนมัติเพื่อให้แน่ใจว่าจะไม่ซ้ำกัน คุณสามารถเพิ่มบันทึกข้อความลงในภาพหน้าจอของคุณโดยอัตโนมัติ หรือจะเลือก
	 โดยเพิ่ม Date:Time ปัจจุบันเป็นส่วนต่อท้ายของสตริงข้อความได้เช่นกัน
	 เมื่อปรากฏขึ้นบนหน้าจอ MTPaint เพียงลาก ข้อความที่คุณ
\ไม่ต้องการให้อยู่ในภาพหน้าจอ หากต้องการ แบบอักษร ขนาด ตำแหน่งและสี
	 ฯลฯ สามารถเลือกได้ในกล่องโต้ตอบ MTPaint Paste Text ภาพหน้าจอ 