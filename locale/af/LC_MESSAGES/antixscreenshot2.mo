��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  &   �  �   �  �   �  �   i  �   #     �     �     �  
   �     �     �  !   �     !  "   9  p   \     �  �   �  �   �  �   �    �      �!     �!  �   �!  �   �"  �   L#  �   4$  %   �$     �$     �$     %  ]   %  
   m%  
   x%  �   �%  �   1&     �&  S  �&  �  $(     $*     ?*  !   Z*  �   |*     <+  #   D+  |   h+  �   �+  w  �,  
   (.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Afrikaans (https://www.transifex.com/antix-linux-community-contributions/teams/121548/af/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: af
Plural-Forms: nplurals=2; plural=(n != 1);
 Aksie Aksie venster: Voeg Datum@tyd by om teks aan te teken Nadat die prent en duim geskep is, verskyn 'n Aksie-venster waar jy dit
	 vertel watter aksies om met jou prent te neem. Aan die bokant sal dit wys of
	 die skermskoot gestoor is of nie, en die gevolglike pad en lêernaam. Nadat die foto geneem is, as dit van die volskerm is, sal dit afgeskaal word
	 tot 85% sodat dit in die redigeringsvenster sal pas en makliker kan wees om te redigeer. 'n Toepassing wat 'n foto van jou skerm neem, laat jou
	 toe om dit te voorbeskou, aan te teken en te wysig, en dit te stoor, asook om dit in te plak of
	op te maak met ander programme. 'n Aanvanklike dialoogvenster laat jou toe om die toepassing te vertel wat jy wil hê. Verander\in\die opsies as jy moet, klik dan OK. Terug Kies 'n ander gids Skep 'n duimnael Wyser Kies Vee bladsy uit: Dialoogvenster: Redigeer en annoteer met MTPaint: Foutboodskap van imgur: Lêer bestaan nie; $lêer oorslaan Lêer gestoor, Gereed om geselekteerde proses uit te voer, skermkiekie gestoor as: $SAVEDIR/$file_name.$file_ext Volskerm Indien gekies, word die prent na die knipbord gekopieer sodat jy dit direk kan plak
	 in ander toepassings (soos Yahoo of AOL e-pos opstel skerms) of dokumente
	 soos LibreOffice Writer, ens., sonder om aan te heg n leêr. As jy die vouer in 'n lêerbestuurder oopmaak, kan jy regskliek op die lêers om
	 te sien watter opsies dit sal toelaat. Die meeste lêerbestuurders sal jou toelaat om die programopsies by te voeg of
	 te verander as dit nie lyk soos jy wil nie. As jy 'n IMGUR-kliënt-API-ID registreer, kan jy jou eie in die blokkie invoer
	 wat verskaf word en die verstekwaarde wat gegee word, ignoreer. As jy probleme ondervind met IMGUR


 nie gereeld werk nie, probeer om jou eie Client API ID te registreer. In Venstermodus, as jy skermkorrupsieprobleme het (zzzFM of SpaceFM
	 lessenaar en Fluxbox kan probleme oplewer), probeer om Venstergrense te ontkies
	 om te sien of dit help. 'n Opsie om die muiswyser self by jou
	 prent in te sluit, is ook beskikbaar in Venstermodus. Sluit muiswyser in Sluit vensterrand in Dit kan jou prent in verskeie tipes lêers stoor, beperk tot dié wat ondersteun word
	 deur die onderliggende foto-neem-toepassing, Scrot, en MTPaint, die program wat gebruik word om
	 die skermkiekies te bekyk, annoteer en redigeer. Dit kan die foto neem van 'n sigbare area wat jy met die muiswyser kies,
\'n sigbare venster, of die hele skerm op 'n vertraging. Dit het die opsie om 'n duim vir jou prent te skep, en dit skep die duim
	 nadat jy jou aanvanklike annotasie en redigering met MTPaint gedoen het. Dit is toelaatbaar
	 om 'n duim meer as 100% van die oorspronklike grootte te skep. Dit sal verstek om 'n Skermkiekies-lêergids in jou tuisgids te skep en te gebruik, maar jy kan dit na 'n ander gids verander as jy wil. LibreOffice is nie geïnstalleer nie. Opening Streek om vas te vang Stoor status: Stoor en maak oop met MTPaint maak beide die skermkiekie en duim oop as 'n duim


 geskep is. Skermskoot Skermskote Sommige toepassings op die spyskaart word generies genoem omdat die werklike toepassing wat


 nie sal loop nie, die huidige verstek is wat in Voorkeurtoepassings gekies is. Die prent sal dan in MTPaint oopmaak sodat u 'n voorskou, redigeer en aantekeninge kan maak. Al
	 MTPaint se kenmerke is beskikbaar. Kleinkiekie vir webblaaie Om bykomende teks by jou prent te voeg, klik net op die T-ikoon en voer die teks
	 in die verskafde veld in. Jy kan die lettergrootte en kenmerke aanpas as jy
	 wil. Klik op die Plak teks-knoppie wanneer jy klaar is. Kies en sleep dan die teks na
	 waar jy dit wil plaas. As dit nie reg is nie, druk Ctrl-z om te ontdoen en probeer
	 weer. Om 'n lyn met 'n pyl aan die einde te trek, klik jy op die Reguitlyn-ikoon wat
	 'n Liniaal is, en wys dan na waar die lyn moet begin en klik, sleep dan
	 na waar daardie gedeelte van die lyn moet eindig. Los die muisknoppie om
	 daardie gedeelte van die lyn te beëindig. Jy kan nog 'n afdeling byvoeg deur te klik en
	 weer te sleep. Nadat die lyngedeelte getrek is, kan jy die A-sleutel
	 druk en dit sal die einde van die lyn in 'n pylpunt verander. Druk dan die Esc
	-sleutel om lyntekenmodus te beëindig. Oplaai na IMGUR het misluk Laai URL op na IMGUR $file Laai tans lêer op na IMGUR $file Wanneer jy klaar is met wysig en annoteer, klik die Stoor-knoppie en dan die Sluit
	-knoppie. Nadat jy uitgegaan het, sal jou duim outomaties gegenereer word vanaf die
	 gestoorde skermskoot. Venster Venster MOET heeltemal sigbaar wees Jy het ook die opsie om net te kanselleer of te verlaat, 'n ander
	 skermskoot te neem, of die lêer te stoor en te verlaat. Jy kan die basisnaam kies van die lêers wat jy geskep wil hê binne stelsel
	 beperkings en dit sal outomaties die naam met die huidige datum
	 en tyd agtervoeg om seker te wees dat dit uniek sal wees. Jy kan dit outomaties 'n teksnota by jou skermkiekie laat voeg, opsioneel
	 met die huidige Datum:Tyd ook as 'n agtervoegsel by jou teksstring gevoeg.
	 Wanneer dit op die MTPaint-skerm verskyn, sleep eenvoudig die teks na waar jy dit
	 op die skermkiekie wil hê. As jy wil, kan die lettertipe, grootte, posisie en kleur,
	 ens gekies word in die MTPaint Paste Text dialoog. skermskoot 