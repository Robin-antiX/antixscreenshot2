��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  ,   �  4    �   C  �   *  �        �     �     �     �     �     �  +     "   =  #   `  �   �       '       F   &  g!  P  �"  "   �#  (   $    +$  �   ;%  �   �%  �   �&     �'  
   �'     �'     �'  n   �'     C(     X(  �   m(  �   )  &   �)  �  �)    �+      .  $   >.  '   c.  �   �.     Z/  7   c/  �   �/  �   0  �  1     �2            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Irish (https://www.transifex.com/antix-linux-community-contributions/teams/121548/ga/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ga
Plural-Forms: nplurals=5; plural=(n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4);
 Gníomh Fuinneog gnímh: Cuir Dáta@Am leis chun an téacs a nótáil Tar éis an pic agus an ordóg a chruthú, tagann fuinneog Gníomhaíochta aníos nuair a insíonn tú dó cad iad na bearta ba cheart a dhéanamh le do phictiúr. Ag barr an leathanaigh taispeánfar cé acu
	 ar sábháladh nó nár sábháladh an seat, agus an cosán agus ainm an chomhaid mar thoradh air. Tar éis don phictiúr a bheith tógtha, más den scáileán iomlán é, déanfar é a laghdú de réir scála 
	 go 85% ionas go mbeidh sé oiriúnach sa fhuinneog eagarthóireachta agus beidh sé níos éasca é a chur in eagar. Ligeann aip a ghlacann pictiúr de do scáileán,



























chás a réamhamharc air, é a anótáil agus a chur in eagar, agus é a shábháil, chomh maith le é a ghreamú isteach nó


\a oscailt le haipeanna eile. Ceadaíonn fuinneog dialóg tosaigh duit a insint don app cad ba mhaith leat. Athraigh\in
a roghanna más gá duit, ansin cliceáil OK. Ar ais Roghnaigh eolaire difriúil Cruthaigh mionsamhail Roghnaigh Cúrsóir Scrios leathanach: Fuinneog dialóige: Eagarthóireacht agus Anótáil le MTPaint: Teachtaireacht earráide ó imgur: Níl an comhad ann; ag bacadh $file Sábháladh an comhad, Réidh le próiseas roghnaithe a dhéanamh, sábháladh gabháil scáileáin mar: $SAVEDIR/$file_name.$file_ext Scáileán iomlán Má roghnaítear é, cóipeáiltear an pictiúr chuig an ngearrthaisce ionas gur féidir leat é a ghreamú go díreach
	 isteach in aipeanna eile (cosúil le scáileáin cumadóireachta ríomhphoist Yahoo nó AOL) nó i ndoiciméid ar nós LibreOffice Writer, srl., gan gá a chur leis. comhad. Má osclaíonn tú an fillteán i mbainisteoir comhad, is féidir leat cliceáil ar dheis ar na comhaid
	 chun na roghanna a cheadóidh sé a fheiceáil. Ligfidh formhór na mbainisteoirí comhad duit roghanna an chláir a chur leis nó

 a athrú mura dtaispeántar iad mar is mian leat. Má chláraíonn tú Aitheantas API Cliant IMGUR, is féidir leat do chuid féin a chur isteach sa bhosca
	 a sholáthair tú ag sárú an luach réamhshocraithe a thugtar. Má bhíonn fadhbanna agat le IMGUR
	 nach n-oibríonn go minic, bain triail as d'aitheantas Cliant API féin a chlárú. I mód Fuinneog, má tá fadhbanna truaillithe scáileáin agat (is féidir le zzzFM nó SpaceFM
	 desktop agus Fluxbox fadhbanna a chruthú), déan iarracht teorainneacha Fuinneog a dhíroghnú
	 féachaint an gcuidíonn sé. Tá rogha chun an cúrsóir luiche féin a chur san áireamh i do phictiúr ar fáil i mód Fuinneog freisin. Cuir cúrsóir luiche san áireamh Cuir teorainn na fuinneoige san áireamh Is féidir leis do phictiúr a shábháil go dtí cineálacha éagsúla comhad, teoranta dóibh siúd a fhaigheann tacaíocht ón aip tógála pictiúr, Scrot, agus MTPaint, an ríomhchlár a úsáidtear chun na screenshots a fheiceáil, a anótáil agus a chur in eagar. Is féidir leis an bpictiúr a ghlacadh de limistéar infheicthe a roghnaíonn tú leis an gcúrsóir luiche,
	a Fuinneog infheicthe, nó an scáileán iomlán ar mhoill. Tá an rogha aige ordóg a chruthú do do phictiúr, agus cruthaíonn sé an ordóg
	 tar éis duit do chuid anótála agus eagarthóireachta tosaigh a dhéanamh le MTPaint. Tá sé incheadaithe
	 ordóg a chruthú os cionn 100% den bhunmhéid. Beidh sé réamhshocraithe chun fillteán Screenshots a chruthú agus a úsáid i do eolaire
	 baile, ach is féidir leat é a athrú go heolaire eile más mian leat. Níl LibreOffice suiteáilte. Ag oscailt Réigiún a ghabháil Sábháil Stádas: Osclaíonn Sábháil agus oscail le MTPaint an seat scáileáin agus an ordóg araon má cruthaíodh
	 ordóg. gabháil scáileáin Seatanna scáileáin Tá roinnt aipeanna ar an roghchlár ainmnithe go cineálach toisc gurb é an aip iarbhír a rithfidh
	 ná an réamhshocrú reatha roghnaithe sna Feidhmchláir Roghnaithe. Osclófar an pictiúr in MTPaint ansin chun tú a réamhamharc, a chur in eagar agus a anótáil. Tá gach
	 de ghnéithe MTPaint ar fáil. Mionsamhail do leathanaigh ghréasáin Chun Téacs breise a chur le do phictiúr, níl le déanamh ach cliceáil ar an deilbhín T agus cuir isteach an téacs
	 sa réimse a chuirtear ar fáil. Is féidir leat an clómhéid agus na tréithe a athrú más mian leat
	. Cliceáil ar an gcnaipe Greamaigh Téacs nuair a dhéantar é. Ansin roghnaigh agus tarraing an téacs go
	 an áit a dteastaíonn uait é a shuíomh. Mura bhfuil sé ceart, brúigh Ctrl-z chun é a chealú agus bain triail eile as
	. Chun líne a tharraingt le saighead ag an deireadh cliceáil tú ar an deilbhín Líne Dhíreach atá ina Rialóir, agus ansin díríonn tú go dtí an áit ar cheart don líne tosú agus cliceáil, ansin tarraing
	 go dtí an áit a bhfuil an chuid sin den líne chóir deireadh. Fág an cnaipe luiche go
	 chun an chuid sin den líne a chríochnú. Is féidir leat rannán eile a chur leis trí chliceáil agus
	 a tharraingt arís. Tar éis an chuid den líne a tharraingt, is féidir leat an eochair A a bhrú
	 agus déanfaidh sé ceann saighde deireadh na líne. Ansin brúigh an eochair Esc
	 chun an modh tarraingthe líne deiridh. Theip ar uaslódáil chuig IMGUR URL á uaslódáil chuig IMGUR $file Comhad á uaslódáil go comhad IMGUR $ Nuair a bheidh an eagarthóireacht agus anótáil déanta, cliceáil ar an gcnaipe Sábháil agus ansin ar an gcnaipe Dún
	. Tar éis duit imeacht ginfear d'ordóg go huathoibríoch ón
	 seat sábháilte. Fuinneog NÍ MÓR an fhuinneog a bheith le feiceáil go hiomlán Tá an rogha agat freisin gan ach cur ar ceal nó imeacht, gabháil scáileáin
	 eile, nó an comhad a shábháil agus imeacht. Is féidir leat bunainm na gcomhad a theastaíonn uait a chruthú laistigh de theorainneacha an chórais a roghnú agus cuirfidh sé an t-ainm leis an dáta reatha agus an t-am go huathoibríoch le bheith cinnte go mbeidh sé uathúil. Is féidir leat nóta téacs a chur le do scáileán go huathoibríoch, go roghnach
	 leis an Dáta reatha:Am curtha leis mar iarmhír le do theaghrán téacs freisin.
	 Nuair a thagann sé suas ar an scáileán MTPaint, tarraing an téacs chuig an áit a dteastaíonn uait é

 ar an scáileán. Más mian leat, is féidir an cló, méid, suíomh agus dath,
	 srl a roghnú sa dialóg MTPaint Paste Text. scléip 