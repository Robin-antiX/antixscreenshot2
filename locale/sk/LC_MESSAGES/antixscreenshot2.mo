��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  %     �   (  �     �   �  �   n          	          3     B     X  $   j     �  %   �  }   �     N  
  ^    i  �   u   $  s!     �"     �"  �   �"     �#  �   +$  �   �$  #   �%  	   �%     �%     �%  _   �%     G&     Y&  �   k&  �   &'     �'  �  �'  "  W)     z+  )   �+  !   �+  �   �+     �,  "   �,  �   �,  �   X-  �  ,.     �/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/121548/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Akcia Akčné okno: Pridajte Date@Time do textu poznámky Po vytvorení obrázka a palca sa zobrazí okno akcie, v ktorom poviete, aké akcie máte s obrázkom vykonať. V hornej časti sa zobrazí, či
	 bola snímka obrazovky uložená alebo nie, a výsledná cesta a názov súboru. Ak je obrázok po nasnímaní na celú obrazovku, zmenší sa
	 na 85 %, takže sa zmestí do okna úprav a bude sa dať ľahšie upravovať. Aplikácia, ktorá odfotí vašu obrazovku, umožní vám
	zobraziť jej ukážku, pridať k nej anotácie, upraviť ju a uložiť, ako aj prilepiť do iných aplikácií alebo ich
	vrieť s inými aplikáciami. Úvodné dialógové okno vám umožňuje povedať aplikácii, čo chcete. V prípade potreby zmeňte\v\možnosti a potom kliknite na tlačidlo OK. späť Vyberte iný adresár Vytvorte miniatúru Vyberte kurzor Odstrániť stránku: Dialógové okno: Úpravy a poznámky pomocou MTPaint: Chybová správa od imgur: Súbor neexistuje; preskočenie $file Súbor uložený, pripravený na vykonanie zvoleného procesu, snímka obrazovky uložená ako: $SAVEDIR/$file_name.$file_ext Celá obrazovka Ak je vybratá, obrázok sa skopíruje do schránky, takže ho môžete priamo prilepiť
	 do iných aplikácií (ako sú obrazovky na napísanie e-mailu Yahoo alebo AOL) alebo dokumentov
	 ako LibreOffice Writer atď., bez toho, aby ste ho museli prikladať súbor. Ak otvoríte priečinok v správcovi súborov, kliknutím pravým tlačidlom myši na súbory zobrazíte
	 aké možnosti to umožňuje. Väčšina správcov súborov vám umožní pridať alebo

 zmeniť možnosti programu, ak sa nezobrazujú tak, ako by ste chceli. Ak zaregistrujete IMGUR Client API Id, môžete zadať svoje vlastné do poskytnutého poľa
	, čím prepíšete predvolenú hodnotu. Ak máte problémy s tým, že IMGUR





 často nefunguje, skúste zaregistrovať svoje vlastné ID klientskeho API. V režime okna, ak máte problémy s poškodením obrazovky (zzzFM alebo SpaceFM
	 desktop a Fluxbox môžu spôsobovať problémy), skúste zrušiť výber hraníc okna
	, aby ste zistili, či to pomôže. Možnosť zahrnúť samotný kurzor myši do obrázka je dostupná aj v režime okna. Zahrnúť kurzor myši Zahrnúť okraj okna Dokáže uložiť váš obrázok do rôznych typov súborov, obmedzených na tie, ktoré
	 podporuje základná aplikácia na fotenie Scrot a MTPaint, program používaný na
	 prezeranie, komentovanie a úpravu snímok obrazovky. Môže odfotiť viditeľnú oblasť, ktorú vyberiete kurzorom myši,
	a viditeľné okno alebo celú obrazovku s oneskorením. Má možnosť vytvoriť palec pre váš obrázok a vytvorí palec
	 potom, čo urobíte počiatočnú anotáciu a úpravu pomocou MTPaint. Je povolené
	 vytvoriť palec väčší ako 100 % pôvodnej veľkosti. Predvolene sa vytvorí a použije priečinok Snímky obrazovky vo vašom domovskom
	 adresári, ale ak chcete, môžete ho zmeniť na iný adresár. LibreOffice nie je nainštalovaný. Otvorenie Región na zachytenie Stav uloženia: Uložiť a otvoriť pomocou MTPaint otvorí snímku obrazovky aj palec, ak sa palec

evytvoril. Snímka obrazovky Snímky obrazovky Niektoré aplikácie v ponuke sú pomenované všeobecne, pretože aktuálna aplikácia, ktorá sa

e spustí, je aktuálna predvolená hodnota vybratá v časti Preferované aplikácie. Obrázok sa potom otvorí v MTPaint, aby ste si ho mohli prezrieť, upraviť a anotovať. Dostupné sú všetky
	 funkcie MTPaint. Miniatúry pre webové stránky Ak chcete k obrázku pridať ďalší text, stačí kliknúť na ikonu T a zadať text
	 do poskytnutého poľa. Ak si to neželáte, môžete upraviť veľkosť písma a atribúty. Po dokončení kliknite na tlačidlo Prilepiť text. Potom vyberte a presuňte text na
	 miesto, kde ho chcete umiestniť. Ak to nie je správne, stlačením klávesov Ctrl-z akciu vrátite späť a skúste
	 znova. Ak chcete nakresliť čiaru so šípkou na konci, kliknite na ikonu Rovná čiara, ktorá
	 je pravítko, a potom ukážte na miesto, kde by mala čiara začínať, kliknite a potom potiahnite
	 na miesto, kde je táto časť čiary by mal skončiť. Uvoľnením tlačidla myši
	 neukončíte túto časť riadku. Ďalšiu sekciu môžete pridať kliknutím a

 opätovným ťahaním. Po nakreslení úseku čiary môžete stlačiť kláves A
	a zmení koniec čiary na šípku. Potom stlačením klávesu Esc
	 ukončite režim kreslenia čiar. Nahrávanie do IMGUR zlyhalo Nahráva sa adresa URL do súboru IMGUR $ Nahráva sa súbor do IMGUR $file Po dokončení úprav a pridávania poznámok kliknite na tlačidlo Uložiť a potom na tlačidlo Zavrieť
	. Po ukončení sa váš palec automaticky vygeneruje z
	 uloženej snímky obrazovky. okno Okno MUSÍ byť úplne viditeľné Máte tiež možnosť jednoducho zrušiť alebo ukončiť prácu, urobiť ďalšiu
	 snímku obrazovky alebo uložiť súbor a skončiť. Môžete si vybrať základný názov súborov, ktoré chcete vytvoriť, v rámci systémových
	 obmedzení a automaticky k názvu pripojí aktuálny dátum
	a čas, aby ste si boli istí, že bude jedinečný. Môžete si nechať automaticky pridať textovú poznámku na vašu snímku obrazovky, voliteľne
	 s aktuálnym dátumom a časom pridaným ako prípona k vášmu textovému reťazcu.
	 Keď sa objaví na obrazovke MTPaint, jednoducho potiahnite text na snímke obrazovky na miesto, kde ho

 nechcete. V prípade potreby je možné zvoliť font, veľkosť, polohu a farbu
	 atď. v dialógovom okne MTPaint Paste Text. snímka obrazovky 