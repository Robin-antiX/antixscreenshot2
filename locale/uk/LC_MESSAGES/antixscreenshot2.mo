��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �         �     �  ?   �  �  �    k  Y  s  �   �  
   �  *   �  #   �        "   -      P   U   o   <   �   .   !  �   1!     �!  �  "  �  �#  �  �%  �  �'  &   �)  $   �)  �  �)  �   �+  �  m,  #  5.  (   Y/     �/  (   �/  "   �/  �   �/     �0     �0  1  �0  �   2  )   �2  �  3  �  �5  0   v9  :   �9  4   �9  O  :  
   g;  B   r;  �   �;  [  k<  N  �=     A            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Ukrainian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Дія Вікно дій: Додайте дату@час до тексту нотатки Після створення зображення та великого пальця з’являється вікно дії, де ви вказуєте

, які дії виконувати з вашим зображенням. Угорі буде показано,

 збережено знімок екрана чи ні, а також отриманий шлях та ім’я файлу. Після зйомки знімка, якщо воно розгорнуто на весь екран, воно буде зменшено
	 до 85%, щоб воно помістилося у вікні редагування та легше редагувати. Програма, яка робить знімок вашого екрана, дозволяє
	попередньо переглядати його, коментувати та редагувати, а також зберігати, а також вставляти його в або
\відкривати з іншими програмами. Початкове діалогове вікно дозволяє вам сказати програмі, що ви хочете. Змініть\у\параметри, якщо потрібно, а потім натисніть OK. Назад Виберіть інший каталог Створіть мініатюру Виберіть курсор Видалити сторінку: Діалогове вікно: Редагування та коментування за допомогою MTPaint: Повідомлення про помилку від imgur: Файл не існує; пропуск $file Файл збережено, готовий до виконання вибраного процесу, знімок екрана збережено як: $SAVEDIR/$file_name.$file_ext Повноекранний Якщо вибрано, зображення копіюється в буфер обміну, щоб ви могли вставити його безпосередньо
	 в інші програми (наприклад, екрани створення електронної пошти Yahoo або AOL) або документи
	, як-от LibreOffice Writer тощо, без необхідності вкладати файл. Якщо ви відкриєте папку в файловому менеджері, ви можете клацнути правою кнопкою миші файли, щоб побачити
	, які параметри він дозволить. Більшість файлових менеджерів дозволять вам додавати або

 змінювати параметри програми, якщо вони виглядають не так, як ви хотіли б. Якщо ви зареєструєте ідентифікатор клієнтського API IMGUR, ви можете ввести свій власний у відповідне поле
	, замінивши задане значення за замовчуванням. Якщо у вас виникли проблеми з IMGUR
	 не працює часто, спробуйте зареєструвати свій власний ідентифікатор API клієнта. У віконному режимі, якщо у вас є проблеми з пошкодженням екрана (zzzFM або SpaceFM
	 робочий стіл і Fluxbox можуть викликати проблеми), спробуйте скасувати вибір меж вікна
	, щоб перевірити, чи допоможе це. У віконному режимі також доступна можливість включити курсор миші у ваше
	 зображення. Включити курсор миші Включити межі вікна Він може зберігати ваше зображення в різних типах файлів, обмежуючись файлами, які
	 підтримуються базовим додатком для фотографування, Scrot і MTPaint, програма, що використовується для

 перегляду, коментування та редагування знімків екрана. Він може зробити знімок видимої області, яку ви вибираєте курсором миші,
\відображеного вікна або всього екрана із затримкою. Він має можливість створення великого пальця для вашого зображення, і він створює великий палець
	 після того, як ви зробите початкові анотації та редагування за допомогою MTPaint. Дозволено
	 створювати великий палець понад 100% від початкового розміру. За замовчуванням буде створено та використовувати папку «Знімки екрана» у вашому домашньому
	 каталозі, але ви можете змінити її на інший каталог, якщо хочете. LibreOffice не встановлено. Відкриття Регіон для захоплення Статус збереження: Зберегти та відкрити за допомогою MTPaint відкриває і знімок екрана, і великий палець, якщо великий палець

 створено. Скріншот Скріншоти Деякі програми в меню мають загальні назви, оскільки фактичний додаток, який
\не запускатиметься, є поточним за умовчанням, вибраним у розділі »Пріоритетні програми«. Зображення відкриється в MTPaint, щоб ви могли попередньо переглянути, відредагувати та додати анотації. Усі
	 функції MTPaint доступні. Ескіз для веб-сторінок Щоб додати додатковий текст до свого зображення, просто клацніть піктограму T і введіть текст
	 у відповідне поле. Ви можете змінити розмір шрифту та атрибути, якщо хочете

. Після завершення натисніть кнопку Вставити текст. Потім виділіть і перетягніть текст
	 у те місце, де ви хочете його розташувати. Якщо це не так, натисніть Ctrl-z, щоб скасувати, і спробуйте
	 знову. Щоб намалювати лінію зі стрілкою в кінці, клацніть значок «Пряма лінія», який
	 є лінійкою, а потім вкажіть на місце, де має починатися лінія, і клацніть, а потім перетягніть
	 туди, де ця частина лінії має закінчитися. Відпустіть кнопку миші, щоб
	 завершити цю ділянку рядка. Ви можете додати інший розділ, натиснувши та
	 перетягнувши знову. Після того, як ділянка лінії буде намальована, ви можете натиснути клавішу A
	, і кінець лінії перетвориться на стрілку. Потім натисніть клавішу Esc
	, щоб завершити режим малювання лінії. Помилка завантаження в IMGUR Завантаження URL-адреси до IMGUR $file Завантаження файлу до IMGUR $file Завершивши редагування та додавання анотацій, натисніть кнопку Зберегти, а потім кнопку Закрити
	. Після виходу з
	 збереженого знімка екрана автоматично згенерується великий палець. Вікно Вікно ПОВИННЕ бути повністю видимим Ви також можете просто скасувати або вийти, зробити інший
	 знімок екрана або зберегти файл і вийти. Ви можете вибрати базову назву файлів, які ви хочете створити, в межах системних
	 обмежень, і воно автоматично додасть до назви поточну дату
	 і час, щоб переконатися, що воно буде унікальним. Ви можете автоматично додавати текстову нотатку до вашого знімка екрана, за бажанням
	 із поточною датою: час, доданою як суфікс до вашого текстового рядка.
	 Коли вона з’явиться на екрані MTPaint, просто перетягніть текст до того місця, де ви






















































































































\n

\ssd кшл на екрані на екрані потрібного місця на знімку екрана. За бажанням у діалоговому вікні MTPaint Paste Text можна вибрати шрифт, розмір, положення та колір,
	 тощо. знімок екрана 