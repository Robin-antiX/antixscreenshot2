��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  -   �  �     �   
  �   �  �   N     �     �                %     2  (   @     i  #   �  p   �         0    @  �   L     $!     7"     N"  �   c"  w   E#  �   �#  �   �$     1%     M%     Y%     n%  x   �%     �%     &  �   &  �   �&     x'  �  �'  2  )     O+  (   o+  )   �+  �   �+     �,      �,  f   �,  �   -  �  �-     �/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Latvian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Darbība Darbības logs: Pievienojiet Date@Time, lai atzīmētu tekstu Kad attēls un īkšķis ir izveidots, tiek parādīts darbības logs, kurā varat norādīt
	, kādas darbības jāveic ar attēlu. Augšdaļā tiks parādīts, vai ekrānuzņēmums ir saglabāts vai nav, kā arī iegūtais ceļš un faila nosaukums. Pēc attēla uzņemšanas, ja tas ir pilnekrāna režīmā, tas tiks samazināts
	 līdz 85%, lai tas ietilptu rediģēšanas logā un būtu vieglāk rediģējams. Lietotne, kas uzņem jūsu ekrāna attēlu, ļauj
	o priekšskatīt, komentēt un rediģēt un saglabāt, kā arī ielīmēt vai
\atvērt ar citām lietotnēm. Sākotnējā dialoglodziņā varat norādīt lietotnei, ko vēlaties. Ja nepieciešams, mainiet opcijas, pēc tam noklikšķiniet uz Labi. Atpakaļ Izvēlieties citu direktoriju Izveidojiet sīktēlu Kursora atlase Dzēst lapu: Dialoga logs: Rediģēšana un anotēšana ar MTPaint: Kļūdas ziņojums no imgur: Fails neeksistē; izlaižot $ failu Fails saglabāts, gatavs veikt atlasīto procesu, ekrānuzņēmums saglabāts kā: $SAVEDIR/$file_name.$file_ext Pilnekrāna režīms Ja tas ir atlasīts, attēls tiek kopēts starpliktuvē, lai jūs varētu to tieši ielīmēt
	 citās lietotnēs (piemēram, Yahoo vai AOL pasta e-pasta rakstīšanas ekrānos) vai dokumentos,
	, piemēram, LibreOffice Writer utt., bez nepieciešamības pievienot fails. Ja atverat mapi failu pārvaldniekā, varat ar peles labo pogu noklikšķināt uz failiem, lai redzētu,
	 kādas opcijas tā atļaus. Lielākā daļa failu pārvaldnieku ļaus jums pievienot vai

 nemainīt programmas opcijas, ja tās neparādās tā, kā vēlaties. Ja reģistrējat IMGUR klienta API ID, lodziņā
	 varat ievadīt savu, ignorējot norādīto noklusējuma vērtību. Ja jums ir problēmas ar IMGUR

 nedarbojas bieži, mēģiniet reģistrēt savu klienta API ID. Ja loga režīmā ir problēmas ar ekrāna bojājumu (zzzFM vai SpaceFM
	 darbvirsma un Fluxbox var radīt problēmas), mēģiniet noņemt loga apmaļu atlasi
	, lai redzētu, vai tas palīdz. Loga režīmā ir pieejama arī iespēja iekļaut savā attēlā
	 peles kursoru. Iekļaut peles kursoru Iekļaut loga apmali Tas var saglabāt attēlu dažāda veida failos, tikai
	 tiem, ko atbalsta pamatā esošā attēlu uzņemšanas lietotne Scrot un MTPaint — programma, ko izmanto, lai
	 skatītu, komentētu un rediģētu ekrānuzņēmumus. Tas var uzņemt redzamā apgabala attēlu, kuru atlasāt ar peles kursoru,
	a redzamu logu vai visu ekrānu ar aizkavi. Tam ir iespēja izveidot īkšķi jūsu attēlam, un tas izveido īkšķi
	 pēc sākotnējās anotēšanas un rediģēšanas ar MTPaint. Ir atļauts
	 izveidot īkšķi, kas pārsniedz 100% no sākotnējā izmēra. Pēc noklusējuma tiks izveidota un izmantota mape Ekrānuzņēmumi jūsu mājas
	 direktorijā, taču, ja vēlaties, varat to mainīt uz citu direktoriju. LibreOffice nav instalēts. Atvēršana Reģions, ko uzņemt Saglabāt statusu: Saglabāt un atvērt, izmantojot MTPaint, tiek atvērts gan ekrānuzņēmums, gan īkšķis, ja

av izveidots īkšķis. Ekrānuzņēmums Ekrānuzņēmumi Dažām izvēlnē esošajām lietotnēm ir vispārīgi nosaukumi, jo faktiskā lietotne, kas
\ nedarbosies, ir pašreizējā noklusējuma programma, kas atlasīta sadaļā Vēlamās lietojumprogrammas. Pēc tam attēls tiks atvērts programmā MTPaint, lai to varētu priekšskatīt, rediģēt un komentēt. Ir pieejamas visas
	 MTPaint funkcijas. Tīmekļa lapu sīktēls Lai attēlam pievienotu papildu tekstu, vienkārši noklikšķiniet uz ikonas T un ievadiet tekstu
	 norādītajā laukā. Ja nevēlaties, varat pielāgot fonta lielumu un atribūtus. Kad esat pabeidzis, noklikšķiniet uz pogas Ielīmēt tekstu. Pēc tam atlasiet un velciet tekstu uz
	, kur vēlaties to novietot. Ja tas nav pareizi, nospiediet Ctrl-z, lai atsauktu, un mēģiniet
	 vēlreiz. Lai zīmētu līniju ar bultiņu beigās, noklikšķiniet uz Taisnas līnijas ikonas, kas
	 ir lineāls, un pēc tam norādiet uz vietu, kur līnijai jāsākas, un noklikšķiniet, pēc tam velciet
	 uz šo līnijas daļu. vajadzētu beigties. Atlaidiet peles pogu, lai

 beigtu šo līnijas daļu. Varat pievienot citu sadaļu, vēlreiz noklikšķinot un

evelkot. Kad līnijas daļa ir novilkta, varat nospiest taustiņu A
	, un līnijas beigas tiks pārvērstas par bultiņas galu. Pēc tam nospiediet taustiņu Esc
	, lai beigtu līnijas vilkšanas režīmu. Augšupielāde IMGUR neizdevās Notiek URL augšupielāde IMGUR $ failā Notiek faila augšupielāde IMGUR $failā Kad rediģēšana un anotācija ir pabeigta, noklikšķiniet uz pogas Saglabāt un pēc tam uz pogas Aizvērt
	. Pēc iziešanas no saglabātā ekrānuzņēmuma
	 automātiski tiks ģenerēts īkšķis. Logs Logam JĀBŪT pilnībā redzamam Varat arī vienkārši atcelt vai iziet, uzņemt citu
	 ekrānuzņēmumu vai saglabāt failu un iziet. Sistēmas
	 ierobežojumu ietvaros varat izvēlēties izveidoto failu pamatnosaukumu, un tas automātiski pievienos nosaukumam pašreizējo datumu
	 un laiku, lai pārliecinātos, ka tas būs unikāls. Varat iestatīt, lai tas automātiski pievienotu teksta piezīmi jūsu ekrānuzņēmumam, pēc izvēles
	 ar pašreizējo Datums:Laiks pievienotu kā sufiksu arī jūsu teksta virknei.
	 Kad tas parādās MTPaint ekrānā, vienkārši velciet tekstu uz vietu, kur
\ nevēlaties to ekrānuzņēmumā. Ja vēlaties, MTPaint teksta ielīmēšanas dialoglodziņā var izvēlēties fontu, izmēru, pozīciju un krāsu,
	 utt. ekrānuzņēmums 