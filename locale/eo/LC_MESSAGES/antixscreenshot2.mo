��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  #   �  �   �  �   �  �   e  x        �     �     �     �     �     �  $   �       +   $  s   P  
   �  �   �  �   �  �   �  '  �      �!     �!  �   "  �   �"  �   _#  �   ?$     �$     �$     �$     %  j   %  
   �%     �%  �   �%  �   )&     �&  w  �&    =(     K*      h*  %   �*  �   �*     h+      q+  l   �+  �   �+  �  �,  
   B.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Esperanto (https://www.transifex.com/antix-linux-community-contributions/teams/121548/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 Ago Aga fenestro: Aldonu Daton@Horon por noti tekston Post kiam la bildo kaj dikfingro estas kreitaj, aperas Ago-fenestro kie vi diras
	 al ĝi, kiajn agojn fari kun via bildo. Supre ĝi montros ĉu
	 la ekrankopio estas konservita aŭ ne, kaj la rezultan vojon kaj dosiernomo. Post kiam la bildo estas prenita, se ĝi estas de la plena ekrano, ĝi estos malgrandigita
	 al 85% do ĝi konvenos en la redakta fenestro kaj estos pli facile redakti. Apo kiu fotas vian ekranon, permesas al vi
\antaŭrigardi ĝin, komentari kaj redakti ĝin, kaj konservi ĝin, kaj ankaŭ alglui ĝin en aŭ
\malfermi kun aliaj aplikaĵoj. Komenca Dialogo-fenestro permesas vin diri al la apo kion vi volas. Ŝanĝu\en\la opcioj se vi bezonas, tiam alklaku Ok. Reen Elektu malsaman dosierujon Kreu bildeton Kursoro Elektu Forigi paĝon: Dialogo-fenestro: Redaktado kaj Komentado per MTPaint: Erarmesaĝo de imgur: Dosiero ne ekzistas; preterpasante $dosiero Dosiero konservita, Preta por plenumi elektitan procezon, ekrankopio konservita kiel: $SAVEDIR/$file_name.$file_ext Plenekrane Se elektita, la bildo estas kopiita al la tondujo por ke vi povu alglui ĝin rekte
	 en aliajn programojn (kiel ekranojn de komponado de retpoŝto de Yahoo aŭ AOL) aŭ dokumentoj
	 kiel LibreOffice Writer, ktp., sen bezoni alfiksi. dosiero. Se vi malfermas la dosierujon en dosieradministranto, vi povas dekstre alklaki la dosierojn por vidi
	 kiajn opciojn ĝi permesos. Plej multaj dosieradministrantoj permesos al vi aldoni aŭ

 ŝanĝi la programopciojn se ili ne aperas kiel vi ŝatus. Se vi registras IMGUR Klienta API-Identigo, vi povas enigi vian propran en la skatolo
	 provizita anstataŭante la defaŭltan valoron donitan. Se vi havas problemojn kun IMGUR

 ne funkcias ofte, provu registri vian propran KlientAPI-Idon. En Fenestra reĝimo, se vi havas ekranajn koruptajn problemojn (zzzFM aŭ SpaceFM
	 labortablo kaj Fluxbox povas prezenti problemojn), provu malelekti Fenestrajn randojn
	 por vidi ĉu ĝi helpas. Opcio por inkluzivi la muskursonon mem en via
	 bildo estas ankaŭ disponebla en Fenestra reĝimo. Inkluzivi muskursoron Inkluzivi fenestran randon Ĝi povas konservi vian bildon al diversaj specoj de dosieroj, limigitaj al tiuj subtenataj
	 de la suba programo de fotado, Scrot kaj MTPaint, la programo uzata por
	 vidi, komentarii kaj redakti la ekrankopiojn. Ĝi povas preni la bildon de videbla areo, kiun vi elektas per la muskursoro,
	a videbla Fenestro, aŭ la tuta ekrano en prokrasto. Ĝi havas la eblon krei dikfingron por via bildo, kaj ĝi kreas la dikfingron
	 post kiam vi faras vian komencan komentadon kaj redaktadon per MTPaint. Estas permesate
	 krei dikfingron pli ol 100% de la originala grandeco. Defaŭlte ĝi kreos kaj uzu dosierujon de Ekrankopioj en via hejma
	 dosierujo, sed vi povas ŝanĝi ĝin al alia dosierujo se vi volas. LibreOffice ne estas instalita. Malfermo Regiono por kapti Konservu Statuson: Konservi kaj malfermi kun MTPaint malfermas kaj la ekrankopion kaj dikfingron se dikfingro estis

 kreita. Ekrankopio Ekrankopioj Iuj aplikaĵoj en la menuo estas nomitaj genre ĉar la reala aplikaĵo, kiu

e ruliĝos, estas la nuna defaŭlta elektita en Preferataj Aplikoj. La bildo tiam malfermos en MTPaint por ke vi antaŭrigardu, redaktu kaj komentu. Ĉiuj
	 el la funkcioj de MTPaint estas disponeblaj. Bildeto por retpaĝoj Por aldoni plian Tekston al via bildo, simple alklaku la T-ikonon kaj enigu la tekston
	 en la provizita kampo. Vi povas ĝustigi la tiparograndon kaj atributojn se vi





 deziras. Alklaku la butonon Alglui Tekston kiam finite. Poste elektu kaj trenu la tekston al
	 kie vi volas ke ĝi estu poziciigita. Se ĝi ne estas ĝusta, premu Ctrl-z por malfari kaj provu
	 denove. Por desegni linion per sago ĉe la fino, vi alklaku la Rektlinio-piktogramon kiu
	 estas Regulo, kaj poste indikas kie la linio devus komenciĝi kaj alklaku, tiam trenu
	 al kie tiu sekcio de la linio. devus fini. Lasu la musbutonon por

 fini tiun sekcion de la linio. Vi povas aldoni alian sekcion alklakante kaj

 trenante denove. Post kiam la sekcio de linio estas desegnita, vi povas premi la A-klavon
	 kaj ĝi turnos la finon de la linio en sagpinton. Poste premu la klavon Esc
	 por fini la reĝimon de desegna linio. Alŝuto al IMGUR malsukcesis Alŝutante URL al IMGUR $dosiero Alŝutante dosieron al IMGUR $dosiero Fininte redaktadon kaj komentarion, alklaku la butonon Konservi kaj poste la butonon Fermi
	. Post kiam vi eliras, via dikfingro aŭtomate estos generita el la
	 konservita ekrankopio. Fenestro Fenestro DEVAS esti tute videbla Vi ankaŭ havas la eblon nur nuligi aŭ eliri, preni alian
	 ekrankopion aŭ konservi la dosieron kaj eliri. Vi povas elekti la bazan nomon de la dosieroj, kiujn vi volas krei en la sistemo
	-limigoj kaj ĝi aŭtomate sufiksos la nomon kun la aktuala dato
	 kaj tempo por certigi, ke ĝi estos unika. Vi povas ke ĝi aŭtomate aldonu tekstan noton al via ekrankopio, laŭvole
	 kun la nuna Dato:Tempo aldonita kiel sufikso ankaŭ al via teksta ĉeno.
	 Kiam ĝi aperas sur la ekrano de MTPaint, simple trenu. la tekston al kie vi



 volas ĝin sur la ekrankopio. Se vi deziras, la tiparo, grandeco, pozicio kaj koloro,
	 ktp povas esti elektitaj en la dialogo de MTPaint Alglui Tekston. ekrankopio 