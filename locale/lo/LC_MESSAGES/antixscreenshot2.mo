��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �  .   �  P   �    8  �  P  t  �  ?  e  $   �   T   �   !   !  '   A!     i!  "   }!  I   �!  7   �!  /   ""  �   R"     9#    L#  N  T%  �  �'  z  r)  ?   �+  6   -,  �  d,  s  0.    �/  ~  �1  4   83     m3  9   z3  %   �3  �   �3     �4     �4  �  �4  D  E6  B   �7  W  �7  �  %;  =   �?  D   @  E   Z@  �  �@     vB  ]   �B  �   �B  �  �C  �  �E     8I            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Lao (https://www.transifex.com/antix-linux-community-contributions/teams/121548/lo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lo
Plural-Forms: nplurals=1; plural=0;
 ການປະຕິບັດ ປ່ອງຢ້ຽມປະຕິບັດ: ເພີ່ມ Date@Time ເພື່ອບັນທຶກຂໍ້ຄວາມ ຫຼັງຈາກການສ້າງຮູບ ແລະໂປ້ໂປ້, ໜ້າຈໍຄຳສັ່ງຈະປາກົດຂຶ້ນບ່ອນທີ່ທ່ານບອກ

ມັນຕ້ອງດຳເນີນການຫຍັງກັບຮູບຂອງເຈົ້າ. ຢູ່ເທິງສຸດ ມັນຈະສະແດງວ່າ
	 ບັນທຶກພາບໜ້າຈໍໄດ້ຫຼືບໍ່, ແລະ ເສັ້ນທາງ ແລະຊື່ໄຟລ໌ທີ່ເປັນຜົນ. ຫຼັງຈາກຮູບພາບຖືກຖ່າຍແລ້ວ, ຖ້າຫາກວ່າມັນເປັນຂອງຫນ້າຈໍເຕັມ, ມັນຈະໄດ້ຮັບການຂະຫຍາຍຕົວລົງ
	 ເປັນ 85% ດັ່ງນັ້ນມັນຈະເຫມາະກັບປ່ອງຢ້ຽມແກ້ໄຂແລະງ່າຍທີ່ຈະແກ້ໄຂ. ແອັບທີ່ຖ່າຍຮູບໜ້າຈໍຂອງທ່ານ, ອະນຸຍາດໃຫ້ທ່ານ
	ສະແດງຕົວຢ່າງມັນ, ໝາຍເຫດ ແລະແກ້ໄຂມັນ, ແລະບັນທຶກມັນ, ພ້ອມທັງວາງມັນໃສ່ ຫຼື
\ເປີດດ້ວຍແອັບອື່ນ. ປ່ອງຢ້ຽມ Dialog ເບື້ອງຕົ້ນອະນຸຍາດໃຫ້ທ່ານບອກ app ສິ່ງທີ່ທ່ານຕ້ອງການ. ປ່ຽນ\ໃນ\ທາງເລືອກ ຖ້າເຈົ້າຕ້ອງການ, ຈາກນັ້ນຄລິກ ຕົກລົງ. ກັບຄືນໄປບ່ອນ ເລືອກໄດເລກະທໍລີທີ່ແຕກຕ່າງກັນ ສ້າງຮູບຫຍໍ້ ເລືອກຕົວກະພິບ ລຶບໜ້າ: ໜ້າຈໍໂຕ້ຕອບ: ການແກ້ໄຂ ແລະໝາຍເຫດດ້ວຍ MTPaint: ຂໍ້ຄວາມຜິດພາດຈາກ imgur: ບໍ່ມີໄຟລ໌; ຂ້າມ $file ບັນທຶກໄຟລ໌ແລ້ວ, ພ້ອມທີ່ຈະດໍາເນີນຂະບວນການທີ່ເລືອກ, ບັນທຶກພາບຫນ້າຈໍເປັນ: $SAVEDIR/$file_name.$file_ext ເຕັມຈໍ ຖ້າເລືອກ, ຮູບພາບຈະຖືກຄັດລອກໃສ່ຄລິບບອດເພື່ອໃຫ້ທ່ານສາມາດວາງມັນໄດ້ໂດຍກົງ
	 ເຂົ້າໄປໃນແອັບຯອື່ນ (ເຊັ່ນ: ໜ້າຈໍການຂຽນອີເມວ Yahoo ຫຼື AOL) ຫຼືເອກະສານ
	 ເຊັ່ນ LibreOffice Writer, ແລະອື່ນໆ, ໂດຍບໍ່ຈໍາເປັນຕ້ອງແນບ. ໄຟລ໌. ຖ້າທ່ານເປີດໂຟນເດີຢູ່ໃນຕົວຈັດການໄຟລ໌, ທ່ານສາມາດຄລິກຂວາໃສ່ໄຟລ໌ເພື່ອເບິ່ງ
	ສິ່ງທີ່ມັນຈະອະນຸຍາດໃຫ້. ຜູ້ຈັດການໄຟລ໌ສ່ວນໃຫຍ່ຈະອະນຸຍາດໃຫ້ທ່ານເພີ່ມ ຫຼື
\ບໍ່ປ່ຽນຕົວເລືອກໂປຣແກຣມ ຖ້າພວກມັນບໍ່ປາກົດໃນແບບທີ່ທ່ານຕ້ອງການ. ຖ້າທ່ານລົງທະບຽນ IMGUR Client API Id, ທ່ານສາມາດໃສ່ຂອງທ່ານເອງໃນປ່ອງ
\ t ທີ່ໃຫ້ overriding ຄ່າເລີ່ມຕົ້ນທີ່ໃຫ້. ຖ້າທ່ານມີບັນຫາກັບ IMGUR

ບໍ່ໄດ້ເຮັດວຽກເລື້ອຍໆ, ໃຫ້ລອງລົງທະບຽນ ID API ລູກຄ້າຂອງທ່ານເອງ. ໃນໂໝດ Window, ຖ້າທ່ານມີບັນຫາການສໍ້ລາດບັງຫຼວງຂອງໜ້າຈໍ (zzzFM ຫຼື SpaceFM
	 desktop ແລະ Fluxbox ສາມາດນຳສະເໜີບັນຫາໄດ້), ໃຫ້ລອງຍົກເລີກການເລືອກຂອບ Window
	 ເພື່ອເບິ່ງວ່າມັນຊ່ວຍໄດ້ຫຼືບໍ່. ທາງເລືອກທີ່ຈະລວມເອົາເຄີເຊີເມົ້າເອງຢູ່ໃນຮູບ
	 ຂອງເຈົ້າຍັງມີຢູ່ໃນໂໝດໜ້າຈໍ. ລວມເອົາຕົວກະພິບຂອງຫນູ ປະກອບມີຂອບປ່ອງຢ້ຽມ ມັນສາມາດບັນທຶກຮູບຂອງເຈົ້າໃສ່ໄຟລ໌ປະເພດຕ່າງໆໄດ້, ຈຳກັດໃຫ້ສະເພາະຕົວທີ່ຮອງຮັບ
	 ໂດຍແອັບຖ່າຍຮູບພື້ນຖານ, Scrot, ແລະ MTPaint, ໂປຣແກຣມທີ່ໃຊ້ເພື່ອ
	ເບິ່ງ, ບັນທຶກສຽງ ແລະແກ້ໄຂຮູບໜ້າຈໍ. ມັນສາມາດເອົາຮູບພາບຂອງພື້ນທີ່ສັງເກດເຫັນທີ່ທ່ານເລືອກດ້ວຍຕົວຊີ້ວັດຂອງຫນູ,
	a ປ່ອງຢ້ຽມທີ່ສາມາດເບິ່ງເຫັນ, ຫຼືທັງຫມົດຫນ້າຈໍໃນການຊັກຊ້າ. ມັນມີທາງເລືອກໃນການສ້າງໂປ້ມືສໍາລັບຮູບພາບຂອງທ່ານ, ແລະມັນຈະສ້າງໂປ້ໂປ້
	 ຫຼັງຈາກທີ່ທ່ານເຮັດຄໍາອະທິບາຍເບື້ອງຕົ້ນຂອງທ່ານແລະການແກ້ໄຂກັບ MTPaint. ມັນອະນຸຍາດ
	 ເພື່ອສ້າງໂປ້ໂປ້ເກີນ 100% ຂອງຂະໜາດຕົ້ນສະບັບ. ມັນຈະເປັນຄ່າເລີ່ມຕົ້ນຂອງການສ້າງ ແລະໃຊ້ໂຟນເດີພາບໜ້າຈໍຢູ່ໃນໄດເຣັກທໍຣີເຮືອນ
	 ຂອງທ່ານ, ແຕ່ທ່ານສາມາດປ່ຽນເປັນໄດເຣັກທໍຣີອື່ນໄດ້ຫາກຕ້ອງການ. LibreOffice ບໍ່ໄດ້ຕິດຕັ້ງ. ເປີດ ພາກພື້ນທີ່ຈະເກັບກໍາ ບັນທຶກສະຖານະ: ບັນທຶກ ແລະເປີດດ້ວຍ MTPaint ເປີດທັງພາບໜ້າຈໍ ແລະໂປ້ໂປ້ ຖ້າໂປ້ມືຖືກສ້າງ

. ພາບໜ້າຈໍ ພາບໜ້າຈໍ ບາງແອັບໃນເມນູແມ່ນຕັ້ງຊື່ທົ່ວໄປເພາະວ່າແອັບຕົວຈິງທີ່ຈະ

ບໍ່ເຮັດວຽກແມ່ນເປັນຄ່າເລີ່ມຕົ້ນທີ່ເລືອກໃນປັດຈຸບັນຢູ່ໃນແອັບພລິເຄຊັນທີ່ຕ້ອງການ. ຫຼັງຈາກນັ້ນ, ຮູບພາບຈະເປີດຢູ່ໃນ MTPaint ສໍາລັບທ່ານເພື່ອສະແດງຕົວຢ່າງ, ແກ້ໄຂ, ແລະຄໍາບັນຍາຍ. ຄຸນສົມບັດທັງໝົດຂອງ MTPaint ແມ່ນມີໃຫ້. ຮູບຕົວຢ່າງສຳລັບໜ້າເວັບ ເພື່ອເພີ່ມຂໍ້ຄວາມໃສ່ຮູບຂອງທ່ານ, ພຽງແຕ່ຄລິກທີ່ໄອຄອນ T ແລະໃສ່ຂໍ້ຄວາມ
	 ໃນຊ່ອງຂໍ້ມູນທີ່ລະບຸໄວ້. ທ່ານສາມາດປັບຂະໜາດຕົວອັກສອນ ແລະຄຸນລັກສະນະຕ່າງໆໄດ້ຖ້າທ່ານ
\ບໍ່ຕ້ອງການ. ກົດປຸ່ມ Paste Text ເມື່ອເຮັດແລ້ວ. ຈາກນັ້ນເລືອກ ແລະລາກຂໍ້ຄວາມໄປໃສ່
	ບ່ອນທີ່ທ່ານຕ້ອງການໃຫ້ມັນຕັ້ງ. ຖ້າມັນບໍ່ຖືກຕ້ອງ, ກົດ Ctrl-z ເພື່ອຍົກເລີກ ແລະລອງ
	ອີກຄັ້ງ. ເພື່ອແຕ້ມເສັ້ນດ້ວຍລູກສອນຢູ່ທ້າຍ, ໃຫ້ຄລິກທີ່ໄອຄອນເສັ້ນຊື່ທີ່
	 ເປັນໄມ້ບັນທັດ, ແລ້ວຊີ້ໄປບ່ອນທີ່ເສັ້ນຄວນເລີ່ມຕົ້ນ ແລະຄລິກ, ຈາກນັ້ນລາກ
	 ໄປບ່ອນທີ່ພາກສ່ວນນັ້ນຂອງເສັ້ນ. ຄວນສິ້ນສຸດ. ປ່ອຍປຸ່ມເມົ້າໄປເພື່ອ
\ບໍ່ສິ້ນສຸດພາກສ່ວນນັ້ນຂອງແຖວ. ທ່ານສາມາດເພີ່ມພາກສ່ວນອື່ນໂດຍການຄລິກ ແລະ
\ບໍ່ລາກອີກເທື່ອຫນຶ່ງ. ຫຼັງຈາກແຕ້ມພາກສ່ວນຂອງເສັ້ນແລ້ວ, ທ່ານສາມາດກົດປຸ່ມ A
	 ແລະມັນຈະປ່ຽນປາຍແຖວໃຫ້ເປັນຫົວລູກສອນ. ຈາກນັ້ນກົດປຸ່ມ Esc
	 ເພື່ອຮູບແບບການແຕ້ມເສັ້ນສິ້ນສຸດ. ອັບໂຫຼດໃສ່ IMGUR ລົ້ມເຫລວ ກຳລັງອັບໂຫລດ URL ໄປໃສ່ IMGUR $file ກຳລັງອັບໂຫລດໄຟລ໌ໃສ່ IMGUR $file ເມື່ອແກ້ໄຂ ແລະໝາຍເຫດສຳເລັດແລ້ວ, ໃຫ້ຄລິກທີ່ປຸ່ມບັນທຶກ ແລະຈາກນັ້ນກົດປຸ່ມປິດ
	. ຫຼັງຈາກທີ່ທ່ານອອກຈາກນິ້ວໂປ້ຂອງທ່ານຈະໄດ້ຮັບການສ້າງອັດຕະໂນມັດຈາກ 
	 ພາບຫນ້າຈໍທີ່ບັນທຶກໄວ້. ປ່ອງຢ້ຽມ ປ່ອງຢ້ຽມຈະຕ້ອງເຫັນໄດ້ຢ່າງສົມບູນ ທ່ານຍັງມີທາງເລືອກພຽງແຕ່ຍົກເລີກ ຫຼືອອກ, ຖ່າຍຮູບໜ້າຈໍ
	 ອື່ນ, ຫຼືບັນທຶກໄຟລ໌ແລ້ວອອກ. ທ່ານສາມາດເລືອກຊື່ພື້ນຖານຂອງໄຟລ໌ທີ່ທ່ານຕ້ອງການສ້າງພາຍໃນຂໍ້ຈໍາກັດຂອງລະບົບ
	 ແລະມັນຈະໃສ່ທ້າຍຊື່ໂດຍອັດຕະໂນມັດດ້ວຍວັນທີປະຈຸບັນ
	 ແລະເວລາເພື່ອໃຫ້ແນ່ໃຈວ່າມັນຈະເປັນເອກະລັກ. ທ່ານສາມາດໃຫ້ມັນເພີ່ມບັນທຶກຂໍ້ຄວາມໃສ່ພາບໜ້າຈໍຂອງທ່ານໂດຍອັດຕະໂນມັດ, ເປັນທາງເລືອກ
	 ດ້ວຍວັນທີ: ເວລາທີ່ເພີ່ມເປັນຄຳຕໍ່ທ້າຍໃຫ້ກັບສະຕຣິງຂໍ້ຄວາມຂອງທ່ານເຊັ່ນກັນ.
	 ເມື່ອມັນຂຶ້ນເທິງໜ້າຈໍ MTPaint, ພຽງແຕ່ລາກ ຂໍ້ຄວາມທີ່ເຈົ້າ
\ບໍ່ຕ້ອງການມັນຢູ່ໃນຮູບໜ້າຈໍ. ຖ້າຕ້ອງການ, ສາມາດເລືອກຕົວອັກສອນ, ຂະໜາດ, ຕຳແໜ່ງ ແລະສີ,
	 ແລະ ອື່ນໆໄດ້ໃນກ່ອງຂໍ້ຄວາມ MTPaint Paste. ພາບໜ້າຈໍ 