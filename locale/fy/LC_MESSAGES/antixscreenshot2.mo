��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  *   �  �   �  �   �  �   y  x     	   �     �     �     �     �     �  #   �     
  $   $  r   I     �  �   �  �   �  �   �  #  �      �!     �!  
  �!  �   �"  �   x#  �   T$     �$     �$      %     %  `    %  
   �%     �%  �   �%  �   0&     �&  {  �&  !  J(     l*     �*  "   �*  �   �*     �+  $   �+  �   �+  �   :,  �  -  
   �.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Western Frisian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/fy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fy
Plural-Forms: nplurals=2; plural=(n != 1);
 Aksje Aksje finster: Foegje Datum@Tiid ta om tekst te notearjen Nei't de foto en tomme binne makke, ferskynt in aksjefinster wêr't jo 
	 fertelle hokker aksjes jo moatte nimme mei jo foto. Oan de boppekant sil it sjen oft
	 de skermôfdruk is bewarre of net, en it resultearjende paad en de triemnamme. Nei't de foto is nommen, as it fan it folslein skerm is, sil it omleech wurde 
	 nei 85%, sadat it yn it bewurkingsfinster past en makliker te bewurkjen is. In app dy't in foto fan jo skerm makket, lit jo
	 it foarbyld besjen, annotearje en bewurkje, en bewarje, en ek plakke yn of
	oopje mei oare apps. In earste dialoochfinster lit jo de app fertelle wat jo wolle. Feroarje\yn\de opsjes as jo nedich binne, klik dan op Ok. Efterkant Kies in oare map Meitsje in thumbnail Cursor selektearje Side wiskje: dialoochfinster: Bewurkje en annotearje mei MTPaint: Flaterberjocht fan imgur: Triem bestiet net; $bestân oerslaan Triem bewarre, klear om selekteare proses út te fieren, skermôfbylding bewarre as: $SAVEDIR/$file_name.$file_ext Folslein skerm As selekteare, wurdt de foto kopiearre nei it klamboerd, sadat jo it direkt kinne plakke
	 yn oare apps (lykas Yahoo of AOL e-post opstelle skermen) of dokuminten
	 lykas LibreOffice Writer, ensfh., sûnder hoege te heakjen in bestân. As jo de map iepenje yn in triembehearder, kinne jo rjochtsklikke op de bestannen om te sjen
	 hokker opsjes it sil tastean. De measte triembehearders kinne jo de programma-opsjes tafoegje of
	 wizigje as se net ferskine sa't jo wolle. As jo in IMGUR Client API Id registrearje, kinne jo jo eigen ynfiere yn it fak
	 opjûn troch de opjûne standertwearde te oerskriuwen. As jo problemen hawwe mei IMGUR
	 net faak wurket, besykje dan jo eigen Client API-ID te registrearjen. As jo yn Finstermodus problemen hawwe mei skermkorrupsje (zzzFM of SpaceFM
	 buroblêd en Fluxbox kinne problemen opleverje), besykje dan Finstergrinzen út te selektearjen
	 om te sjen oft it helpt. In opsje om de mûsoanwizer sels yn jo
	 foto op te nimmen is ek beskikber yn Finstermodus. Meitsje mûsoanwizer Ynklusyf finsterrâne It kin jo foto opslaan yn ferskate soarten bestannen, beheind ta dyjingen dy't wurde stipe
	 troch de ûnderlizzende app foar it meitsjen fan foto's, Scrot, en MTPaint, it programma dat wurdt brûkt om
	 de skermôfbyldings te besjen, te annotearjen en te bewurkjen. It kin de foto nimme fan in sichtber gebiet dat jo selektearje mei de mûsoanwizer,
	a sichtber Finster, of it hiele skerm mei fertraging. It hat de opsje om in tomme te meitsjen foar jo foto, en it makket de tomme
	 nei't jo jo earste annotaasje en bewurking hawwe dien mei MTPaint. It is tastien
	 om in tomme te meitsjen oer 100% fan 'e orizjinele grutte. It sil standert in skermôfbyldingsmap oanmeitsje en brûke yn jo thúsmap
	, mar jo kinne it feroarje yn in oare map as jo wolle. LibreOffice is net ynstalleare. Iepening Regio te fangen Status opslaan: Bewarje en iepenje mei MTPaint iepenet sawol de skermôfdruk as de tomme as in tomme
	 makke is. Skermprint Skermôfbyldings Guon apps yn it menu wurde generysk neamd, om't de eigentlike app dy't
	 sil rinne de hjoeddeiske standert is selektearre yn Foarkarsapplikaasjes. De foto sil dan iepenje yn MTPaint foar jo om te besjen, te bewurkjen en te annotearjen. Alle
	 funksjes fan MTPaint binne beskikber. Miniatuer foar websiden Om ekstra tekst ta te foegjen oan jo foto, klikje jo gewoan op it T-ikoantsje en typ de tekst
	 yn it levere fjild. Jo kinne de lettertypegrutte en attributen oanpasse as jo
	 wolle. Klikje op de knop Tekst plakke as klear. Selektearje en sleep dan de tekst nei
	 wêr't jo dizze pleatse wolle. As it net goed is, druk dan op Ctrl-z om ûngedien te meitsjen en besykje
	 nochris. Om in line te tekenjen mei in pylk oan 'e ein, klikje jo op it rjochte line-ikoan dat
	 in liniaal is, en wiist dan nei wêr't de line moat begjinne en klikje, slepe dan
	 nei wêr't dat diel fan 'e line moat einigje. Lit de mûsknop los om
	 dat diel fan de rigel te beëinigjen. Jo kinne in oare seksje tafoegje troch te klikken en
	 nochris te slepen. Nei't de seksje fan 'e rigel tekene is, kinne jo op de A-toets drukke
	 en it sil it ein fan 'e rigel yn in pylkpunt feroarje. Druk dan op de Esc
	-kaai om de line tekenmodus te beëinigjen. Upload nei IMGUR mislearre Upload URL nei IMGUR $bestân Bestân opladen nei IMGUR $bestân As it bewurkjen en annotearjen klear is, klikje jo op de knop Bewarje en dan op de knop Slute
	. Nei it ferlitten sil jo tomme automatysk oanmakke wurde fan 'e
	 bewarre skermôfbylding. Finster Finster MOET folslein sichtber wêze Jo hawwe ek de opsje om gewoan te annulearjen of te ferlitten, in oare
	 skermôfbylding te nimmen, of it bestân op te slaan en te gean. Jo kinne de basisnamme kieze fan de triemmen dy't jo wolle oanmakke binnen systeem
	 beheiningen en it sil de namme automatysk efterheakselje mei de aktuele datum
	 en tiid om der wis fan te wêzen dat it unyk is. Jo kinne it automatysk in tekstnotysje oan jo skermôfbylding taheakje, opsjoneel
	 mei de aktuele Datum:Tiid ek tafoege as efterheaksel oan jo tekststring.
	 As it op it MTPaint-skerm komt, slepe jo gewoanwei de tekst nei wêr't jo
	 it wolle op it skermôfbylding. As jo wolle, kinne it lettertype, grutte, posysje en kleur,
	 ensfh wurde keazen yn it dialoochfinster MTPaint Tekst plakke. skermprint 