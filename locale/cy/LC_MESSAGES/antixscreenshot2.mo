��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �    	   �     �  '   �  �     �     �   �  �   "     �     �  	   �     �     �               3  &   J  r   q     �  �   �    �    �    �      "     /"  �   G"  x   #  �   �#  �   H$     �$     �$     %     %  S    %     t%     }%  �   �%  q   0&     �&  j  �&    -(     ;*  $   V*  &   {*  �   �*     Y+  &   b+  a   �+  �   �+  m  �,     .            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Welsh (https://www.transifex.com/antix-linux-community-contributions/teams/121548/cy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cy
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 Gweithred Ffenestr weithredu: Ychwanegu Dyddiad@Amser i nodi'r testun Ar ôl i'r llun a'r bawd gael eu creu, mae ffenestr Gweithredu yn ymddangos lle rydych chi'n dweud wrtho pa gamau i'w cymryd gyda'ch llun. Ar y brig bydd yn dangos a yw'r sgrin wedi'i gadw ai peidio, a'r llwybr canlyniadol ac enw'r ffeil. Ar ôl i'r llun gael ei dynnu, os yw o'r sgrin lawn, bydd yn cael ei leihau i 85% fel y bydd yn ffitio yn y ffenestr olygu ac yn haws ei olygu. Ap sy'n tynnu llun o'ch sgrin, sy'n caniatáu i chi











\'w rhagolwg, ei anodi a\'i olygu, a\'i gadw







	 agor gydag apiau eraill. Mae ffenestr Deialog cychwynnol yn caniatáu ichi ddweud wrth yr app beth rydych chi ei eisiau. Newid\in\yr opsiynau os oes angen, yna cliciwch Iawn. Yn ol Dewiswch gyfeiriadur gwahanol Creu bawd Dewis Cyrchwr Dileu tudalen: Ffenestr deialog: Golygu ac Anodi gyda MTPaint: Neges gwall gan imgur: Nid yw'r ffeil yn bodoli; hepgor $file Ffeil wedi'i chadw, Yn barod i gyflawni'r broses ddethol, sgrinlun wedi'i chadw fel: $SAVEDIR/$file_name.$file_ext Sgrin llawn Os caiff ei ddewis, caiff y llun ei gopïo i'r clipfwrdd fel y gallwch ei gludo'n uniongyrchol i mewn i apiau eraill (fel sgriniau cyfansoddi e-bost Yahoo neu AOL) neu ddogfennau
 fel LibreOffice Writer, ac ati, heb fod angen eu hatodi ffeil. Os byddwch yn agor y ffolder mewn rheolwr ffeiliau, gallwch dde-glicio ar y ffeiliau i weld

 pa opsiynau y bydd yn eu caniatáu. Bydd y rhan fwyaf o reolwyr ffeiliau yn caniatáu ichi ychwanegu neu newid opsiynau'r rhaglen os nad ydyn nhw'n ymddangos fel yr hoffech chi. Os ydych chi'n cofrestru ID API Cleient IMGUR, gallwch chi roi eich un chi yn y blwch

 a ddarperir gan ddiystyru'r gwerth rhagosodedig a roddwyd. Os ydych yn cael problemau gydag IMGUR



 ddim yn gweithio'n aml, ceisiwch gofrestru eich ID API Cleient eich hun. Yn y modd Ffenestr, os oes gennych chi broblemau llygredd sgrin (gall penbwrdd zzzFM neu SpaceFM a Fluxbox gyflwyno problemau), ceisiwch ddad-ddewis borderi Ffenestr


 i weld a yw'n helpu. Mae opsiwn i gynnwys cyrchwr y llygoden ei hun yn eich llun hefyd ar gael yn y modd Ffenestr. Cynnwys cyrchwr llygoden Cynnwys border ffenestr Gall arbed eich llun i wahanol fathau o ffeiliau, wedi'u cyfyngu i'r rhai a gefnogir gan yr ap tynnu lluniau sylfaenol, Scrot, a MTPaint, y rhaglen a ddefnyddir i weld, anodi a golygu'r sgrinluniau. Gall gymryd y llun o ardal weladwy a ddewiswch gyda chyrchwr y llygoden,
	a ffenestr weladwy, neu'r sgrin gyfan ar oedi. Mae ganddo'r opsiwn o greu bawd ar gyfer eich llun, ac mae'n creu'r bawd
	 ar ôl i chi wneud eich anodi a golygu cychwynnol gyda MTPaint. Caniateir
	 creu bawd dros 100% o'r maint gwreiddiol. Bydd yn rhagosodedig i greu a defnyddio ffolder Screenshots yn eich cyfeiriadur cartref, ond gallwch ei newid i gyfeiriadur gwahanol os dymunwch. Nid yw LibreOffice wedi'i osod. Agoriad Rhanbarth i ddal Cadw Statws: Mae Cadw ac agor gyda MTPaint yn agor y sgrinlun a'r bawd os cafodd bawd ei greu
	. Sgrinlun Sgrinluniau Mae rhai apiau ar y ddewislen wedi'u henwi'n generig oherwydd mai'r ap gwirioneddol a fydd yn rhedeg yw'r rhagosodiad cyfredol a ddewiswyd yn y Cymwysiadau a Ffefrir. Yna bydd y llun yn agor yn MTPaint i chi gael rhagolwg, golygu ac anodi. Mae pob un o nodweddion MTPaint ar gael. Bawdlun ar gyfer tudalennau gwe I ychwanegu Testun ychwanegol i'ch llun, cliciwch yr eicon T a rhowch y testun
	 yn y maes a ddarperir. Gallwch addasu maint y ffont a'r priodoleddau os ydych yn dymuno. Cliciwch ar y botwm Gludo Testun pan fydd wedi'i wneud. Yna dewiswch a llusgwch y testun i
	 lle rydych chi am iddo gael ei leoli. Os nad yw'n iawn, pwyswch Ctrl-z i ddadwneud a cheisio
	 eto. I dynnu llinell gyda saeth ar y diwedd rydych yn clicio ar yr eicon Straight Line sy

	 yn Pren mesur, ac yna pwyntio at ble ddylair llinell ddechrau a chlicio, yna llusgo
	 i ble maer rhan honno or llinell dylai ddod i ben. Gollyngwch fotwm y llygoden i

 gorffen yr adran honno o'r llinell. Gallwch ychwanegu adran arall drwy glicio

	 llusgo eto. Ar ôl i'r rhan o'r llinell gael ei thynnu, gallwch wasgu'r fysell A
	 a bydd yn troi diwedd y llinell yn ben saeth. Yna pwyswch yr allwedd Esc

	 i fodd tynnu llinell derfyn. Methodd uwchlwytho i IMGUR Wrthi'n uwchlwytho URL i IMGUR $file Wrthi'n uwchlwytho ffeil i IMGUR $file Pan fyddwch wedi gorffen golygu ac anodi, cliciwch y botwm Cadw ac yna'r botwm Cau
	. Wedi i chi adael, bydd eich bawd yn cael ei gynhyrchu'n awtomatig o'r sgrinlun sydd wedi'i gadw. Ffenestr RHAID i'r ffenestr fod yn gwbl weladwy Mae gennych hefyd yr opsiwn o ganslo neu adael, cymryd sgrinlun arall, neu gadw'r ffeil a gadael. Gallwch ddewis enw sylfaen y ffeiliau rydych chi am eu creu o fewn cyfyngiadau'r system a bydd yn ôl-ddodiad yr enw yn awtomatig gyda'r dyddiad cyfredol
	 ac amser i sicrhau y bydd yn unigryw. Gallwch ei gael i ychwanegu nodyn testun i'ch sgrin yn awtomatig, yn ddewisol
	 gyda'r dyddiad cyfredol: Amser wedi'i ychwanegu fel ôl-ddodiad i'ch llinyn testun hefyd.
	 Pan ddaw i fyny ar y sgrin MTPaint, llusgwch y testun i'r lle rydych chi ei eisiau ar y sgrin. Os dymunir, gellir dewis y ffont, maint, lleoliad a lliw,
	 ac ati yn yr ymgom MTPaint Paste Text. sgrinlun 