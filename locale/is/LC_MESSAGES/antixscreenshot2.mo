��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  ,   �      �     �   �  �   �               )     =     M     [  &   c     �  !   �  m   �     0    >  �   @    4   /  S!     �"     �"  �   �"  �   �#  �   C$  �   B%     �%     �%     �%     
&  h   &  	   �&  	   �&  �   �&  �   ,'     �'  �  �'  "  m)     �+  $   �+     �+  �   �+     �,  (   �,  p   �,  �   f-  �  :.  	   �/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Icelandic (https://www.transifex.com/antix-linux-community-contributions/teams/121548/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 Aðgerð Aðgerðargluggi: Bættu við Date@Time við athugasemdartexta Eftir að myndin og þumalfingur eru búnar til birtist aðgerðargluggi þar sem þú segir
	 hvaða aðgerðir eigi að grípa til með myndinni þinni. Efst mun það sýna hvort
	 skjámyndin hafi verið vistuð eða ekki, og slóðin og skráarnafnið sem af því leiðir. Eftir að myndin er tekin, ef hún er á öllum skjánum, mun hún minnka
	 niður í 85% svo hún passi inn í vinnslugluggann og auðveldara sé að breyta henni. Forrit sem tekur mynd af skjánum þínum, gerir þér
	að forskoða hann, skrifa athugasemdir og breyta honum og vista hann, ásamt því að líma hann inn í eða
\opna með öðrum forritum. Upphafsgluggi gerir þér kleift að segja forritinu hvað þú vilt. Breyttu\í\valkostunum ef þú þarft, smelltu síðan á Í lagi. Til baka Veldu aðra möppu Búðu til smámynd Velja bendilinn Eyða síðu: Gluggi: Breyting og athugasemdir með MTPaint: Villuboð frá imgur: Skrá er ekki til; sleppir $skrá Skrá vistuð, tilbúin til að framkvæma valið ferli, skjámynd vistuð sem: $SAVEDIR/$file_name.$file_ext Fullur skjár Ef valið er, er myndin afrituð á klemmuspjaldið svo þú getir límt hana beint
	 inn í önnur forrit (eins og Yahoo eða AOL tölvupóst sem skrifa skjái) eða skjöl
	 eins og LibreOffice Writer, o.s.frv., án þess að þurfa að hengja við skrá. Ef þú opnar möppuna í skráastjóra geturðu hægrismellt á skrárnar til að sjá
	 hvaða valkosti hún leyfir. Flestir skráarstjórar leyfa þér að bæta við eða
	 breyta forritsvalkostunum ef þeir birtast ekki eins og þú vilt. Ef þú skráir IMGUR Client API auðkenni geturðu slegið inn þitt eigið í reitinn
	 sem kveður á um og hnekkir sjálfgefnu gildinu sem gefið er upp. Ef þú átt í vandræðum með að IMGUR
	 virkar ekki oft skaltu prófa að skrá þitt eigið API auðkenni viðskiptavinar. Í gluggaham, ef þú átt í vandræðum með skjáspillingu (zzzFM eða SpaceFM
	 skjáborð og Fluxbox geta valdið vandamálum), reyndu að afvelja gluggaramma
	 til að sjá hvort það hjálpi. Valkostur til að hafa músarbendilinn sjálfan í
	 myndinni þinni er einnig fáanlegur í gluggaham. Láttu músarbendil fylgja með Láttu gluggaramma fylgja með Það getur vistað myndina þína í ýmsar gerðir skráa, takmarkað við þær sem studdar eru
	 af undirliggjandi myndatökuforritinu, Scrot og MTPaint, forritinu sem er notað til að


	 að skoða, skrifa athugasemdir og breyta skjámyndunum. Það getur tekið mynd af sýnilegu svæði sem þú velur með músarbendlinum,
\a sýnilegan glugga eða allan skjáinn með töf. Það hefur möguleika á að búa til þumalfingur fyrir myndina þína, og það býr til þumalinn
	 eftir að þú gerir fyrstu athugasemdir og breytingar með MTPaint. Það er leyfilegt
	 að búa til þumalfingur yfir 100% af upprunalegri stærð. Það mun sjálfgefið búa til og nota Skjámyndamöppu á heima
	 möppunni þinni, en þú getur breytt henni í aðra möppu ef þú vilt. LibreOffice er ekki uppsett. Opnun Svæði til að fanga Vista stöðu: Vista og opna með MTPaint opnar bæði skjámynd og þumalfingur ef þumalfingur var
\ ekki búinn til. Skjáskot Skjáskot Sum forrit á valmyndinni eru nefnd almennt vegna þess að raunverulegt forrit sem mun


 ekki keyra er sjálfgefið sem valið er í valinn forrit. Myndin mun þá opnast í MTPaint svo þú getir forskoðað, breytt og skrifað athugasemdir. Allir
	 eiginleikar MTPaint eru tiltækir. Smámynd fyrir vefsíður Til að bæta viðbótartexta við myndina þína skaltu bara smella á T táknið og slá inn textann
	 í reitnum sem gefinn er upp. Þú getur breytt leturstærð og eiginleikum ef þú

	 vilt. Smelltu á Paste Text hnappinn þegar því er lokið. Veldu síðan og dragðu textann 
	 þangað sem þú vilt hafa hann staðsettan. Ef það er ekki rétt, ýttu á Ctrl-z til að afturkalla og reyndu
	 aftur. Til að teikna línu með ör í lokin smellirðu á beina línutáknið sem
	 er reglustiku og bendir síðan á hvar línan á að byrja og smellir, dregur síðan
	 þangað sem sá hluti línunnar ætti að enda. Slepptu músarhnappinum til að
	 enda þann hluta línunnar. Þú getur bætt við öðrum hluta með því að smella og

	 draga aftur. Eftir að línuhlutinn hefur verið teiknaður geturðu ýtt á A takkann
	 og það mun breyta enda línunnar í örvahaus. Ýttu síðan á Esc
	 takkann til að ljúka línuteikningarham. Upphleðsla á IMGUR mistókst Hleður upp vefslóð á IMGUR $file Hleður skrá í IMGUR $file Þegar búið er að breyta og skrifa athugasemdir, smelltu á Vista hnappinn og síðan á Loka
	 hnappinn. Eftir að þú hættir verður þumalfingur þinn sjálfkrafa búinn til úr
	 vistuðu skjámyndinni. Gluggi Gluggi VERÐUR að vera alveg sýnilegur Þú hefur líka möguleika á að hætta við eða hætta, taka aðra
	 skjámynd eða vista skrána og hætta. Þú getur valið grunnnafn skránna sem þú vilt búa til innan kerfis
	 takmarkana og það mun sjálfkrafa bæta nafninu við núverandi dagsetningu
	 og tíma til að vera viss um að það verði einstakt. Þú getur látið það sjálfkrafa bæta textaskýrslu við skjámyndina þína, valfrjálst
	 með núverandi Date:Time bætt við sem viðskeyti við textastrenginn þinn líka.
	 Þegar það kemur upp á MTPaint skjánum skaltu einfaldlega draga textinn þangað sem þú

	 vilt hafa hann á skjámyndinni. Ef þess er óskað er hægt að velja leturgerð, stærð, staðsetningu og lit,
	 osfrv í MTPaint Paste Text glugganum. skjáskot 