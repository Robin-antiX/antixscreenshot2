��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  &     �   ,  �     �   �  �   U     �     �     �               ,  $   =     b  (   }  r   �       �   )    )  �   5   5  3!     i"     "  �   �"  �   �#  �   $  �   �$      �%     �%     �%     �%  ]   �%     >&     P&  �   b&  �   '     �'  ~  �'    Q)      m+  *   �+  #   �+  �   �+     �,      �,  w   �,  �   D-  �  .     �/            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Czech (https://www.transifex.com/antix-linux-community-contributions/teams/121548/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Akce Akční okno: Přidejte Date@Time do textu poznámky Po vytvoření obrázku a palce se objeví okno Akce, ve kterém řeknete, jaké akce se mají s obrázkem provést. V horní části se zobrazí, zda
	 byl snímek obrazovky uložen nebo ne, a výsledná cesta a název souboru. Po pořízení obrázku, pokud je na celou obrazovku, bude zmenšen
	 na 85 %, takže se vejde do okna úprav a bude snazší jej upravovat. Aplikace, která vyfotí vaši obrazovku, umožní vám
	zobrazit náhled, přidat k ní poznámky a upravit ji a uložit, stejně jako ji vložit do nebo
	open s jinými aplikacemi. Úvodní dialogové okno vám umožňuje sdělit aplikaci, co chcete. V případě potřeby změňte\v\možnosti a klikněte na OK. Zadní Vyberte jiný adresář Vytvořte miniaturu Vyberte kurzor Smazat stránku: Dialogové okno: Úpravy a poznámky pomocí MTPaint: Chybová zpráva od imgur: Soubor neexistuje; přeskakování $file Soubor uložen, připraven provést vybraný proces, snímek obrazovky uložen jako: $SAVEDIR/$file_name.$file_ext Celá obrazovka Pokud je vybráno, obrázek se zkopíruje do schránky, takže jej můžete přímo vložit
	 do jiných aplikací (jako Yahoo nebo AOL obrazovky pro psaní e-mailu) nebo dokumentů
	 jako LibreOffice Writer atd., aniž byste je museli přikládat soubor. Pokud složku otevřete ve správci souborů, můžete na soubory kliknout pravým tlačítkem
	 a zjistit, jaké možnosti to umožní. Většina správců souborů vám umožní přidat nebo

eměnit možnosti programu, pokud se nezobrazují tak, jak byste chtěli. Pokud zaregistrujete IMGUR Client API Id, můžete zadat své vlastní do poskytnutého pole
	 a přepsat tak danou výchozí hodnotu. Pokud máte problémy s tím, že IMGUR




 často nefunguje, zkuste zaregistrovat své vlastní ID klientského API. V režimu Window, pokud máte problémy s poškozením obrazovky (zzzFM nebo SpaceFM
	 desktop a Fluxbox mohou představovat problémy), zkuste zrušit výběr Window borders
	, abyste viděli, jestli to pomůže. Možnost zahrnout do svého obrázku
	 samotný kurzor myši je také dostupná v režimu okna. Zahrnout kurzor myši Zahrnout okraj okna Může uložit váš obrázek do různých typů souborů, omezených na ty, které
	 podporuje základní aplikace pro fotografování, Scrot a MTPaint, program používaný k
	 prohlížení, přidávání poznámek a úpravě snímků obrazovky. Může pořídit snímek viditelné oblasti, kterou vyberete kurzorem myši,
	a viditelného okna nebo celé obrazovky se zpožděním. Má možnost vytvořit pro váš obrázek palec a vytvoří palec
	 poté, co provedete počáteční poznámky a úpravy pomocí MTPaint. Je povoleno
	 vytvořit palec větší než 100 % původní velikosti. Ve výchozím nastavení bude vytvořena a používána složka Screenshots ve vašem domovském
	 adresáři, ale pokud chcete, můžete ji změnit na jiný adresář. LibreOffice není nainstalován. Otevírací Oblast k zachycení Stav uložení: Uložit a otevřít pomocí MTPaint otevře snímek obrazovky i palec, pokud nebyl vytvořen. Snímek obrazovky Snímky obrazovky Některé aplikace v nabídce jsou pojmenovány obecně, protože skutečná aplikace, která se

e spustí, je aktuální výchozí nastavení vybrané v části Preferované aplikace. Obrázek se poté otevře v MTPaint, abyste si jej mohli prohlédnout, upravit a přidat k němu poznámky. Všechny
	 funkce MTPaintu jsou dostupné. Miniatura pro webové stránky Chcete-li k obrázku přidat další text, stačí kliknout na ikonu T a zadat text
	 do poskytnutého pole. Pokud si to nepřejete, můžete upravit velikost písma a atributy. Po dokončení klikněte na tlačítko Vložit text. Potom vyberte a přetáhněte text na
	 místo, kde jej chcete umístit. Pokud to není v pořádku, stiskněte Ctrl-z pro návrat a zkuste to
	 znovu. Chcete-li nakreslit čáru se šipkou na konci, klikněte na ikonu Přímá čára,
	 je Pravítko, a poté ukažte na místo, kde by měla čára začínat, klikněte a poté přetáhněte
	 na místo, kde je tato část čáry by měl skončit. Uvolněním tlačítka



 ukončíte tuto část řádku. Další sekci můžete přidat kliknutím a

 dalším tažením. Po nakreslení úseku čáry můžete stisknout klávesu A
	a přemění konec čáry na šipku. Poté stiskněte klávesu Esc
	 pro ukončení režimu kreslení čar. Nahrání do IMGUR se nezdařilo Nahrávání adresy URL do souboru IMGUR $ Nahrávání souboru do IMGUR $file Po dokončení úprav a přidávání poznámek klikněte na tlačítko Uložit a poté na tlačítko Zavřít
	. Po ukončení se váš palec automaticky vygeneruje z
	 uloženého snímku obrazovky. Okno Okno MUSÍ být zcela viditelné Máte také možnost pouze zrušit nebo ukončit, pořídit
	 další snímek obrazovky nebo soubor uložit a ukončit. Můžete si vybrat základní název souborů, které chcete vytvořit, v rámci systémových
	 omezení a automaticky se k názvu připojí aktuální datum
	a čas, aby bylo jisté, že bude jedinečný. Můžete si nechat automaticky přidat textovou poznámku k vašemu snímku obrazovky, volitelně
	 s aktuálním datem:časem přidaným jako přípona k vašemu textovému řetězci.
	 Když se objeví na obrazovce MTPaint, jednoduše přetáhněte text na místo, kde jej na snímku obrazovky

 nechcete. V případě potřeby lze v dialogovém okně Vložit text MTPaint vybrat písmo, velikost, pozici a barvu
	 atd. Snímek obrazovky 