��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  (   �  �   �  �   �  �   �  �   S     �     �          "     4     G  -   \     �  (   �  x   �     G  �   S  '  P    x   Q  �!     �"     �"    #  �   $  �   �$  �   �%     )&     F&     M&     d&  X   y&  
   �&     �&  �   �&  �   �'     (  �  4(  3  �)     ,     ,,  "   K,  �   n,     6-  "   ;-  �   ^-  �   �-  �  �.  
   ^0            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Kurdish (https://www.transifex.com/antix-linux-community-contributions/teams/121548/ku/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ku
Plural-Forms: nplurals=2; plural=(n != 1);
 Çalakî Pencereya çalakiyê: Demjimêr@Demê nivîsê lê zêde bikin Piştî ku wêne û tilikê têne çêkirin, pencereyek Çalakiyê derdikeve ku hûn jê re dibêjin ku hûn bi wêneya xwe re çi karan bikin. Li jor ew ê nîşan bide ka wêneyê
	 hatî hilanîn an na, û rê û navê pelê encam dide. Piştî ku wêne were kişandin, heke ew ji ekrana tevahî be, ew ê ji %85 kêm bibe
	 ji ber vê yekê ew ê di pencereya guherandinê de cîh bigire û sererastkirina wê hêsantir be. Serlêdanek ku wêneyek dîmendera we dikişîne, dihêle hûn
	 wê pêşdîtin bikin, şîrove bikin û biguherînin, û hilînin, û hem jî bi sepanên din ve bixin nav an
	û vekin. Pencereyek Dialogê ya destpêkê dihêle hûn ji sepanê re bibêjin ka hûn çi dixwazin. Ger hewce bike vebijarkan biguherîne, paşê bikirtîne Ok. Paş Peldanka cûda hilbijêrin Tîrêjek biafirînin Cursor Hilbijêre Rûpelê jêbirin: Pencereya diyalogê: Bi MTPaint re sererastkirin û şîrovekirin: Peyama çewtiyê ji imgur: File does not exist; pelê $ derbas dibe Dosya hat hilanîn, Amade ye ku pêvajoyek hilbijartî pêk bîne, wêneya dîmenê wekî: $SAVEDIR/$file_name.$file_ext Full Screen Ger were hilbijartin, wêne li pabloyê tê kopî kirin da ku hûn rasterast
	 wê li serîlêdanên din (wek ekranên berhevkirina e-nameya Yahoo an AOL) an belgeyên

 mîna LibreOffice Writer, hwd., bêyî ku hewceyî pêvekirinê bikin bixin pelek. Ger hûn peldankê di rêvebirek pelan de vekin, hûn dikarin pelan rast bikirtînin da ku bibînin
	 ka ew ê destûrê bide kîjan vebijarkan. Piraniya gerînendeyên pelan dê bihêlin ku hûn vebijarkên bernameyê zêde bikin an

 biguhezînin heke ew bi awayê ku hûn dixwazin xuya nakin. Ger hûn Nasnameya API-ya Xerîdar a IMGUR-ê tomar bikin, hûn dikarin ya xwe di nav qutîkê de binivîsin
	 ku ji nirxa xwerû ya hatî dayîn derbas dibe. Ger pirsgirêkên we bi IMGUR

 pir caran naxebite, biceribînin ku Nasnameya API-ya Xerîdar ya xwe tomar bikin. Di moda pencereyê de, ger pirsgirêkên we yên xirabûna ekranê hebin (zzzFM an SpaceFM

 sermaseya û Fluxbox dikare pirsgirêkan derxe holê), biceribînin ku tixûbên pencereyê hilnebijêrin
	 da ku bibînin ka ew dibe alîkar. Vebijêrkek ku hûn nîşana mişkê bixwe di wêneyê
	 we de bihewîne di moda Paceyê de jî heye. Kursorê mişkê têde Sînorê pencereyê tê de Ew dikare wêneya we li cûrbecûr pelan hilîne, bi yên ku
	 ji hêla sepana wênekêşandina wêneyê ya bingehîn, Scrot û MTPaint ve têne piştgirî kirin, bernameya ku

 ji bo dîtin, şîrovekirin û guherandina dîmenan tê bikar anîn sînordar e. Ew dikare wêneya deverek xuyayî ya ku hûn hildibijêrin bi nîşana mişkê,
	a Pencereya xuyakirî, an jî tevahiya ekranê di dereng de bikişîne. Vebijarka wê heye ku ji bo wêneya we tilikê çêbike, û piştî ku hûn bi MTPaint şîrovekirin û sererastkirina xweya destpêkê bikin tilikê
	 diafirîne. Destûr e
	 ji sedî 100% ji mezinahiya orjînal re tiliyek çêbike. Ew ê di peldanka
	 mala we de peldankek Screenshots biafirîne û bikar bîne, lê heke hûn bixwazin hûn dikarin wê biguhezînin peldankek cûda. LibreOffice nayê saz kirin. Dergeh Herêma ku were girtin Rewşa tomarkirinê: Bi MTPaint hilîne û veke heke tilikê

 çêbibe hem dîmen û hem jî tilikê vedike. Screenshot Screenshots Hin sepanên li ser menuyê bi gelemperî têne nav kirin ji ber ku sepana rastîn a ku dê

 bixebite, xwerûya heyî ya ku di Serlêdanên Preferred de hatî hilbijartin e. Dûv re wêne dê di MTPaint-ê de vebe ku hûn pêşdîtin, biguherînin û şîrove bikin. Hemû
	 taybetmendiyên MTPaint hene. Thumbnail ji bo malperan Ji bo ku Nivîsarek din li wêneya xwe zêde bikin, tenê li ser îkona T-yê bikirtînin û nivîsa
	 li qada hatî peyda kirin têkevin. Ger tu bixwazî tu dikarî mezinahiya tîpan û taybetmendiyan eyar bikî. Dema ku pêk hat, bişkojka Nivîsar Bixebitîne. Dûv re nivîsê hilbijêrin û kaş bikin
	 cihê ku hûn dixwazin lê bi cih bibin. Heke ne rast e, Ctrl-z bikirtînin da ku vebikin û dîsa biceribînin
	. Ji bo xêzek bi tîrekê li dawiyê xêz bikin, hûn îkona Xeta Rast a ku
	 Rêvebir e bikirtînin, û dûv re nîşan bidin ku rêz divê li ku derê dest pê bike û bikirtînin, dûv re
	 kaş bikin ka ew beşa rêzê. divê bi dawî bibe. Dest ji bişkoka mişkê berdin ku

 ew beşa rêzê biqede. Hûn dikarin bi tikandin û

 dîsa kaşkirin beşek din lê zêde bikin. Piştî ku beşa rêzê hat xêzkirin, hûn dikarin bişkoja A
	 bixin û ew ê dawiya rêzê bibe serê tîrê. Dûv re bişkoja Esc
	 bikirtînin da ku moda xêzkirina rêzê biqede. Barkirin bo IMGUR têk çû Barkirina URL li pelê IMGUR $ Dosya li IMGUR $pelê tê barkirin Dema ku guherandin û şîrovekirin qediya, bişkoka Save bikirtînin û paşê bişkoka Girtin
	 bikirtînin. Piştî ku hûn derkevin tiliya we dê bixweber ji
	 dîmenê tomarkirî were çêkirin. Pace DIVÊ Pencere bi tevahî xuya bibe Di heman demê de vebijarka we heye ku hûn tenê betal bikin an derkevin, dîmenek
	 din bigirin, an pelê hilînin û derkevin. Hûn dikarin navê bingehîn ê pelên ku hûn dixwazin di nav sînorên pergalê
	 de werin afirandin hilbijêrin û ew ê bixweber navê xwe bi tarîx
	 û demjimêra heyî veqetîne da ku hûn pê bawer bin ku ew ê bêhempa be. Hûn dikarin wê bixweber notek nivîsê li dîmendera xwe zêde bikin, bi awayekî vebijarkî
	 Dîroka niha:Dem jî wekî paşgirek li rêzika nivîsa xwe were zêdekirin.
	 Dema ku ew derket ser ekrana MTPaint, bi tenê kaş bikin nivîsa li cihê ku hûn

 nexwazin li ser dîmenderê. Ger bixwaze, tîp, mezinahî, pozîsyon û reng,
	 hwd dikarin di diyaloga MTPaint Paste Text de werin hilbijartin. screenshot 