��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �  #   �  @   �  �  7  �   �  G  �    :  
   >  8   I  4   �     �      �  %   �  <      *   Y   Y   �   �   �      �!  �  �!  �  s#  �  s%  >  '  <   Z)  B   �)  �  �)  �   �+  r  l,  �   �-  )   �.     /      /     :/  �   W/     �/     0  &  60  �   ]1  -   >2  �  l2  �  ,5  5    9  Q   V9  9   �9  _  �9     B;  G   S;  �   �;  �  �<  �  >     A            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Macedonian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/mk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
 Акција Прозорец за акција: Додајте Date@Time на текстот за белешка Откако ќе се креираат сликата и палецот, се појавува прозорец со акција каде што ќе му кажете што дејствија да преземе со вашата слика. На врвот ќе се прикаже дали
	 сликата од екранот е зачувана или не, како и добиената патека и име на датотека. Откако ќе се направи фотографијата, ако е на цел екран, ќе се намали
	 на 85% за да се вклопи во прозорецот за уредување и полесно да се уредува. Апликација што го фотографира вашиот екран, ви овозможува
	 да ја прегледате, да ја прибележете и уредувате и да ја зачувате, како и да ја залепите или

\отворите со други апликации. Почетниот прозорец за дијалог ви овозможува да и кажете на апликацијата што сакате. Променете ги\во\ опциите ако ви треба, а потоа кликнете Ок. Назад Изберете различен директориум Направете минијатурна слика Изберете курсор Избриши страница: Прозорец за дијалог: Уредување и забележување со MTPaint: Порака за грешка од imgur: Датотеката не постои; прескокнување на $датотека Датотеката е зачувана, подготвена да го изврши избраниот процес, снимката од екранот е зачувана како: $SAVEDIR/$file_name.$file_ext Цел екран Ако е избрана, сликата се копира на таблата со исечоци за да можете директно
	 да ја залепите во други апликации (како екраните за пишување е-пошта на Yahoo или AOL) или документи

 како LibreOffice Writer итн., без потреба да се прикачуваат датотека. Ако ја отворите папката во менаџер на датотеки, можете да кликнете со десното копче на датотеките за да видите
	 кои опции ќе ги дозволи. Повеќето менаџери на датотеки ќе ви овозможат да ги додадете или

 да ги менувате опциите на програмата ако тие не се појавуваат како што сакате. Ако регистрирате ID на клиентски API на IMGUR, можете да го внесете вашиот во полето
	 предвидено со надминување на зададената вредност. Ако имате проблеми со тоа што IMGUR

 не работи често, обидете се да регистрирате сопствен ID на клиент API. Во режимот на прозорец, ако имате проблеми со оштетувањето на екранот (zzzFM или SpaceFM

 десктопот и Fluxbox може да претставуваат проблеми), обидете се да ги отселектирате границите на прозорецот
	 за да видите дали помага. Опција за вклучување на самиот курсор на глувчето во вашата слика е достапна и во режим на прозорец. Вклучете го курсорот на глувчето Вклучете ја границата на прозорецот Може да ја зачува вашата слика во различни типови датотеки, ограничени на оние поддржани
	 од основната апликација за фотографирање, Scrot и MTPaint, програмата што се користи за

 прегледување, бележење и уредување на сликите од екранот. Може да фотографира видлива област што ќе ја изберете со курсорот на глувчето,

 видлив прозорец или целиот екран со задоцнување. Има опција да креира палец за вашата слика и го создава палецот
	 откако ќе го направите вашето првично прибележување и уредување со MTPaint. Дозволено е

 да се создаде палец над 100% од оригиналната големина. Стандардно ќе креира и користи папка Слики од екранот во вашиот дом
	 директориум, но можете да ја промените во друг директориум ако сакате. LibreOffice не е инсталиран. Отворање Регион за снимање Зачувај статус: Зачувај и отворај со MTPaint ги отвора и сликите од екранот и палецот ако

 е создаден палец. Слика од екранот Слики од екранот Некои апликации во менито се именувани генерички затоа што вистинската апликација што нема да работи е тековното стандардно избрано во Претпочитани апликации. Сликата потоа ќе се отвори во MTPaint за да можете да ја прегледате, уредувате и прибележете. Сите
	 функции на MTPaint се достапни. Сликичка за веб-страници За да додадете дополнителен текст на вашата слика, само кликнете на иконата T и внесете го текстот
	 во даденото поле. Ако сакате, можете да ја прилагодите големината на фонтот и атрибутите. Кликнете на копчето Вметни текст кога ќе завршите. Потоа изберете и повлечете го текстот до
	 каде што сакате да биде поставен. Ако не е во ред, притиснете Ctrl-z за да го вратите и обидете се повторно
	. За да нацртате линија со стрелка на крајот, кликнете на иконата Права линија која
	 е Правилник, а потоа покажувате каде треба да започне линијата и кликнете, потоа повлечете
	 до каде тој дел од линијата треба да заврши. Оставете го копчето на глувчето за

 да го завршите тој дел од линијата. Можете да додадете друг дел со кликнување и

 повторно влечење. Откако ќе се нацрта делот од линијата, можете да го притиснете копчето A
	 и ќе го претвори крајот на линијата во врв на стрелка. Потоа притиснете го копчето Esc
	 за да го завршите режимот за цртање линија. Поставувањето на IMGUR не успеа УРЛ-адресата се поставува на датотеката IMGUR $ Се поставува датотека во IMGUR $file Кога ќе завршите со уредувањето и прибелешката, кликнете на копчето Зачувај, а потоа на копчето Затвори
	. Откако ќе излезете, палецот автоматски ќе се генерира од
	 зачуваната слика од екранот. Прозорец Прозорецот МОРА да биде целосно видлив Имате и опција само да откажете или излезете, да направите друга
	 слика од екранот или да ја зачувате датотеката и да излезете. Можете да го изберете основното име на датотеките што сакате да се креираат во рамките на системските ограничувања и тоа автоматски ќе го додаде името со тековниот датум
	 и време за да бидете сигурни дека ќе биде единствено. Може да го поставите автоматски да додава текстуална белешка на вашата слика од екранот, по избор
	 со додадена тековната Date:Time како наставка на вашата текстуална низа исто така.
	 Кога ќе се појави на екранот MTPaint, едноставно повлечете текстот до местото каде што

 не го сакате на сликата од екранот. Доколку сакате, фонтот, големината, положбата и бојата,
	 итн може да се изберат во дијалогот за залепување текст MTPaint. скриншот 