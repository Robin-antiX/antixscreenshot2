��    3      �  G   L      h     i     p       �   �  �   �  �     w   �     1     6     Q     d     r       $   �     �  "   �  a   �     R  �   ^  �   @	  �   *
              ,  �   B  �     �   �  �   j     �          #     5  Z   B  
   �     �  �   �  x   D     �  L  �     !     "     9     V  �   t     $  !   +  v   M  �   �  p  �  
   �  �       �     �  '     �   ,  �     �   �  �   e     �     �          !     2     E  -   X     �  "   �  t   �     ;  �   H    C  �   U   A  9!     {"     �"  �   �"  z   �#  �   $  �   �$     h%  	   �%     �%     �%  V   �%  
   
&     &  �   !&  �   �&     H'  l  a'    �(     �*     �*  "   +  �   A+     ,      ,  {   -,  �   �,  g  -  
   �.            2   '             	       %          -      ,                                 
             3                   *                    0   !   .         "       (   1          &          $       )              /   #              +                  Action Action window: Add Date@Time to note text After the pic and thumb are created, an Action window pops up where you tell\n\t it what actions to take with your pic.  At the top it will display whether\n\t the screenshot has been saved or not, and the resulting path and filename. After the pic is taken, if it is of the full screen, it will be scaled down\n\t to 85% so it will fit in the editing window and be easier to edit. An app that takes a picture of your screen, allows you\n\tto preview it, annotate and edit it, and save it, as well as paste it into or\n\topen with other apps. An initial Dialog window allows you to tell the app what you want. Change\n\tthe options if you need to, then click Ok. Back Choose different directory Create a thumbnail Cursor Select Delete page: Dialog window: Editing and Annotating with MTPaint: Error message from imgur: File doesn't exist; skipping $file File saved, Ready to perform selected process, screenshot saved as: $SAVEDIR/$file_name.$file_ext Full Screen If selected, the pic is copied to the clipboard so you can paste it directly\n\t into other apps (like Yahoo or AOL mail email compose screens) or documents\n\t like LibreOffice Writer, etc., without needing to attach a file. If you open the folder in a file manager, you can right click the files to see\n\t what options it will allow.  Most file managers will allow you to add or\n\t change the program options if they don’t appear the way you would like. If you register an IMGUR Client API Id, you can enter your own in the box\n\t provided overriding the default value given. If you have problems with IMGUR\n\t not working often, try registering your own Client API Id. In Window mode, if you have screen corruption problems (zzzFM or SpaceFM\n\t desktop and Fluxbox can present problems), try unselecting Window borders\n\t to see if it helps.  An option to include the mouse cursor itself in your\n\t picture is also available in Window mode. Include mouse cursor Include window border It can save your picture to various types of files, limited to those supported\n\t by the underlying picture taking app, Scrot, and MTPaint, the program used to\n\t view, annotater and edit the screenshots. It can take the picture of a visible area you select with the mouse cursor,\n\ta visible Window, or the whole screen on a delay. It has the option of creating a thumb for your pic, and it creates the thumb\n\t after you do your initial annotating and editing with MTPaint.  It’s allowable\n\t to create a thumb over 100% of the original size. It will default to creating and using a Screenshots folder in your home\n\t directory, but you can change it to a different directory if you like. LibreOffice is not installed. Opening Region to capture Save Status: Save and open with MTPaint opens both the screenshot and thumb if a thumb was\n\t created. Screenshot Screenshots Some apps on the menu are named generically because the actual app that will\n\t run is the current default selected in Preferred Applications. The pic will then open in MTPaint for you to preview, edit, and annotate. All\n\t of MTPaint’s features are available. Thumbnail for webpages To add additional Text to your pic, just click the T icon and enter the text\n\t in the field provided. You can adjust the font size and attributes if you\n\t wish. Click the Paste Text button when done. Then select and drag the text to\n\t where you want it positioned.  If it isn’t right, press Ctrl-z to undo and try\n\t again. To draw a line with an arrow at the end you click the Straight Line icon which\n\t is a Ruler, and then point to where the line should begin and click, then drag\n\t to where that section of the line should end. Let go of the mouse button to\n\t end that section of the line.  You can add another section by clicking and\n\t dragging again.  After the section of line is drawn, you can press the A key\n\t and it will turn the end of the line into an arrowhead.  Then press the Esc\n\t key to end line draw mode. Upload to IMGUR failed Uploading URL to IMGUR $file Uploading file to IMGUR $file When done editing and annotating, click the Save button and then the Close\n\t button. After you exit your thumb will automatically be generated from the\n\t saved screenshot. Window Window MUST be completely visible You also have the option of just cancelling or exiting, taking another\n\t screenshot, or saving the file and exiting. You can choose the base name of the files you want created within system\n\t limitations and it will automatically suffix the name with the current date\n\t and time to be sure it will be unique. You can have it automatically add a text note to your screenshot, optionally\n\t with the current Date:Time added as a suffix to your text string as well.\n\t When it comes up on the MTPaint screen, simply drag the text to where you\n\t want it on the screenshot.  If desired, the font, size, position and color,\n\t etc can be chosen in the MTPaint Paste Text dialog. screenshot Project-Id-Version: antiXscreenshot2 Ver. 1.08b
Report-Msgid-Bugs-To: https://www.antixforum.com
PO-Revision-Date: 2022-06-18 09:28+0000
Last-Translator: BobC, 2022
Language-Team: Bosnian (https://www.transifex.com/antix-linux-community-contributions/teams/121548/bs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bs
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Akcija Prozor akcije: Dodajte datum@vrijeme u tekst bilješke Nakon što su slika i palac kreirani, pojavljuje se prozor za radnju gdje mu govorite koje radnje treba poduzeti sa svojom slikom. Na vrhu će prikazati da li je

 snimak ekrana sačuvan ili ne, i rezultirajuća putanja i naziv datoteke. Nakon snimanja slike, ako je na cijelom ekranu, smanjit će se
	 na 85% tako da će stati u prozor za uređivanje i biti lakše za uređivanje. Aplikacija koja snima sliku vašeg ekrana, omogućava vam
	 da je pregledate, stavite komentare i uredite je, te sačuvate, kao i da je zalijepite u ili
\otvorite sa drugim aplikacijama. Početni prozor dijaloga vam omogućava da aplikaciji kažete šta želite. Promijenite\u\opcije ako je potrebno, a zatim kliknite Ok. Natrag Odaberite drugi direktorij Kreirajte sličicu Odaberite kursor Izbriši stranicu: Dijaloški prozor: Uređivanje i označavanje pomoću MTPaint-a: Poruka o grešci od imgur-a: Fajl ne postoji; preskakanje $file Fajl je sačuvan, spreman za izvođenje odabranog procesa, snimak ekrana sačuvan kao: $SAVEDIR/$file_name.$file_ext Cijeli ekran Ako je odabrana, slika se kopira u međuspremnik tako da je možete direktno zalijepiti
	 u druge aplikacije (kao što su Yahoo ili AOL ekrani za pisanje e-pošte) ili dokumente
	 kao što je LibreOffice Writer, itd., bez potrebe za prilaganjem fajl. Ako otvorite fasciklu u menadžeru datoteka, možete kliknuti desnim tasterom miša na fajlove da vidite
	 koje opcije će dozvoliti. Većina upravitelja datoteka će vam omogućiti da dodate ili
	 promijenite opcije programa ako se ne pojavljuju onako kako biste željeli. Ako registrujete IMGUR Client API ID, možete unijeti svoj vlastiti u okvir
	 koji je naveden nadjačavajući zadanu vrijednost. Ako imate problema sa IMGUR
	 ne radi često, pokušajte registrirati svoj vlastiti Client API ID. U načinu rada Window, ako imate problema sa oštećenjem ekrana (zzzFM ili SpaceFM
	 radna površina i Fluxbox mogu predstavljati probleme), pokušajte poništiti odabir okvira prozora
	 da vidite da li će to pomoći. Opcija uključivanja samog kursora miša u vašu
	 sliku je također dostupna u načinu rada prozora. Uključuje kursor miša Uključuje okvir prozora Može sačuvati vašu sliku u različite tipove datoteka, ograničeno na one koje podržava osnovna aplikacija za snimanje slika, Scrot i MTPaint, program koji se koristi za

 pregled, označavanje i uređivanje snimaka ekrana. Može snimiti sliku vidljivog područja koje odaberete kursorom miša,
	a vidljivog prozora ili cijelog ekrana sa odgodom. Ima opciju kreiranja palca za vašu sliku, a stvara palac
	 nakon što izvršite svoje početno označavanje i uređivanje sa MTPaint-om. Dozvoljeno je
	 kreiranje palca preko 100% originalne veličine. Zadano će kreirati i koristiti folder Snimci ekrana u vašem početnom
	 direktoriju, ali ga možete promijeniti u drugi direktorij ako želite. LibreOffice nije instaliran. Otvaranje Region za snimanje Sačuvaj status: Sačuvaj i otvori sa MTPaint-om otvara i snimak ekrana i palac ako je palac

 kreiran. Screenshot Screenshots Neke aplikacije u meniju imaju generički naziv jer je stvarna aplikacija koja se

eće pokrenuti trenutno odabrana zadana vrijednost u Preferred Applications. Slika će se zatim otvoriti u MTPaint-u da biste je mogli pregledati, uređivati i komentarisati. Dostupne su sve
	 funkcije MTPainta. Sličica za web stranice Da dodate dodatni tekst vašoj slici, samo kliknite na ikonu T i unesite tekst
	 u predviđeno polje. Možete podesiti veličinu fonta i atribute ako
	 želite. Kliknite na dugme Paste Text kada završite. Zatim odaberite i prevucite tekst na
	 mjesto na kojem želite da bude pozicioniran. Ako nije ispravno, pritisnite Ctrl-z da poništite i pokušajte
	 ponovo. Da nacrtate liniju sa strelicom na kraju, kliknite na ikonu ravna linija koja
	 je ravnalo, a zatim pokažite na mjesto gdje bi linija trebala početi i kliknite, a zatim povucite
	 do mjesta na kojem je taj dio linije treba završiti. Pustite dugme miša da
	 završite taj dio linije. Možete dodati još jedan odjeljak klikom i
	 prevlačenjem ponovo. Nakon što je dio linije nacrtan, možete pritisnuti tipku A
	 i to će pretvoriti kraj linije u vrh strelice. Zatim pritisnite tipku Esc
	 da završite način crtanja linije. Prijenos na IMGUR nije uspio Učitavanje URL-a u IMGUR $file Učitavanje datoteke u IMGUR $file Kada završite sa uređivanjem i označavanjem, kliknite na dugme Sačuvaj, a zatim na dugme Zatvori
	. Nakon što izađete, vaš palac će se automatski generirati iz
	 sačuvanog snimka ekrana. Prozor Prozor MORA biti potpuno vidljiv Također imate opciju da jednostavno otkažete ili izađete, napravite drugi
	 snimak ekrana ili sačuvate fajl i izađete. Možete odabrati osnovno ime fajlova koje želite da kreirate u okviru sistemskih
	 ograničenja i ono će automatski dodati naziv sa trenutnim datumom
	 i vremenom kako biste bili sigurni da će biti jedinstveno. Možete automatski dodati tekstualnu bilješku vašem snimku ekrana, opciono
	 sa trenutnim datumom: Vrijeme dodati kao sufiks vašem tekstualnom nizu.
	 Kada se pojavi na ekranu MTPaint, jednostavno prevucite tekst do mjesta gdje


 želite na snimku ekrana. Po želji, font, veličina, pozicija i boja,
	 itd. mogu se odabrati u dijalogu MTPaint Paste Text. screenshot 