# antiXscreenshot2

Screenshot tool for antiX, providing some post processing options.

## Contributing
- Improvement of translations are always welcome, please register (for free) at transifex and go to [antiX-linux community contributions at transifex](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/antixscreenshot2/) to improve the translation of the user interface in your language if you find the wording or grammar inadequate.
- Questions, suggestions and bug reporting for the recent version of _antixscreenshot2_ please to [antiX forums](https://www.antixforum.com/).

## Authors and acknowledgment
Improved from original antiXscreenshot script by BobC and Robin.

**License:**
GPL Version 3 (GPLv3)

------
Robin.antiX, 2022
